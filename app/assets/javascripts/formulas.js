// Autor: Carlos Andrés Torres Cruz
//
// Fecha creacion: 23-11-2015
//
// Autor actualizacion:
//
// Fecha actualizacion:
//
// Calcula el imc del sistema musculoesqueletico
function sm_imc(){
    var talla = $("#historia_medica_talla").val();
    var peso = $("#historia_medica_peso").val();
    var tipo = $("#historia_medica_unidad_talla").val();
    if(talla == ""){
      talla = 1;
    }
    // si son centímetros se convierte a metro
    if(tipo == 2) {
      talla = talla/100;
    }

    var total = peso / (talla * talla);
    total = Math.round( total * 10 ) / 10;
    $("#historia_medica_imc").val(total);
}

function sm_imc_pris(){
    var talla = $("#sistema_musculoesqueletico_talla").val();
    var peso = $("#sistema_musculoesqueletico_peso").val();
    var tipo = $("#sistema_musculoesqueletico_unidad_talla").val();
    if(talla == ""){
      talla = 1;
    }
    // si son centímetros se convierte a metro
    if(tipo == 2) {
      talla = talla/100;
    }

    var total = peso / (talla * talla);
    total = Math.round( total * 10 ) / 10;
    $("#sistema_musculoesqueletico_imc").val(total);
}

// Autor: Carlos Andrés Torres Cruz
//
// Fecha creacion: 24-11-2015
//
// Autor actualizacion:
//
// Fecha actualizacion:
//
// Calcula la grasa localizada del sistema musculoesqueletico
function sm_grasa_localizada(genero){
    var const_1, const_2;
    if (genero == "F"){
        const_1 = 0.1429;
        const_2 = 4.56;
    }else{
        const_1 = 0.097;
        const_2 = 3.64;
    }
    var triceps = $("#sistema_musculoesqueletico_adipometrium_attributes_triceps").val();
    var s_scap = $("#sistema_musculoesqueletico_adipometrium_attributes_s_escap").val();
    var s_iliaco = $("#sistema_musculoesqueletico_adipometrium_attributes_s_iliaco").val();
    var abdomen = $("#sistema_musculoesqueletico_adipometrium_attributes_abdomen").val();
    var muslo = $("#sistema_musculoesqueletico_adipometrium_attributes_muslo").val();
    var pierna = $("#sistema_musculoesqueletico_adipometrium_attributes_pierna").val();
    var suma = parseFloat(triceps) + parseFloat(s_scap) + parseFloat(s_iliaco) + parseFloat(abdomen) + parseFloat(muslo) + parseFloat(pierna);
    var total = suma * const_1 + const_2;
    $("#sistema_musculoesqueletico_grasa_localizada").val(total);
}

// Autor: Carlos Andrés Torres Cruz
//
// Fecha creacion: 29-11-2015
//
// Autor actualizacion:
//
// Fecha actualizacion:
//
// calcular el total del edema sistema musculoesqueletico

function sm_total_edema (event){
    var value = event.path[0].value;
    var name_id = event.path[0].id;
    var right_id = name_id.split('sistema_musculoesqueletico_edemas_attributes_')[1];
    var id = right_id.split('_')[0];
    var derecho = $("#sistema_musculoesqueletico_edemas_attributes_"+id+"_derecho").val();
    var izquierdo = $("#sistema_musculoesqueletico_edemas_attributes_"+id+"_izquierdo").val();
    var total = Math.abs(parseFloat(derecho) - parseFloat(izquierdo));
    $("#sistema_musculoesqueletico_edemas_attributes_"+id+"_total").val(total);
    var grado;
    if(total <= 1.9){
        grado = "LEVE";
    }else if(total >=2 && total<= 3.9){
        grado= "MODERADO";
    }else{
        grado= "SEVERO";
    }
    $("#sistema_musculoesqueletico_edemas_attributes_"+id+"_grado").val(grado);
}