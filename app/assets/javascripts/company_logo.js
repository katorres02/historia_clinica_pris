/*
Autor: Carlos Andres Torres Cruz

Fecha creacion: 17-11-2015

Autor actualizacion:

Fecha actualizacion:

Metodo: ejecuta la funcionalidad de carga de archivos para ser guardados
mas adelante
*/
function getFile(){
    document.getElementById("upfile").click();
}

/*
Autor: Carlos Andres Torres Cruz

Fecha creacion: 17-11-2015

Autor actualizacion:

Fecha actualizacion:

Metodo: Guarda el logo de la empresa del usuario
*/
function save_company_logo(img, user_id){
    convertToDataURLviaCanvas(img, function(base64Img){
        var data = {
            user_id: user_id,
            image_company: base64Img
            };
        $.post( "/services/save_company_logo", data, function( response ) {
            //console.log(response);
        });
    });
}

/*
Autor: Carlos Andres Torres Cruz

Fecha creacion: 17-11-2015

Autor actualizacion:

Fecha actualizacion:

Metodo: convierte una imagen a formato 'base64' para ser enviada en un json
y ser almacenada en el backend
 */
function convertToDataURLviaCanvas(url, callback, outputFormat){
    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function(){
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var dataURL;
        canvas.height = this.height;
        canvas.width = this.width;
        ctx.drawImage(this, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        callback(dataURL);
        canvas = null;
    };
    img.src = url;
}