// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require turbolinks
//= require bootstrap-sprockets
//= require jquery_nested_form

// TEMA AdminLTE V2
//= require adminlte/theme/app

// lIbrerias AdminLTE V2
//= require adminlte/libraries/pace
//= require adminlte/libraries/jquery.dataTables
//= require adminlte/libraries/jquery.dataTableAdminLTE
//= require adminlte/libraries/bootstrap-datepicker

//= require adminlte/general
//= require formulas
//= require patients

//= require datepicker_es.js


