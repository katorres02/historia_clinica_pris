// Autor: Carlos Andres Torres Cruz
//
// Fecha creacion: 22-01-2018
//
// Autor actualizacion:
//
// Fecha actualizacion:
//
// descativa la opción de editar todos los campos de todos los formularios de la vista
function disableInputs(obj) {
  if(obj == undefined || obj == null){ obj = ""; }
  $("input, textarea, select" + obj).attr('disabled', true);
  $("input[type='submit']").css({'display': 'none'});
}


// Autor: Carlos Andres Torres Cruz
//
// Fecha creacion: 12-07-2017
//
// Autor actualizacion:
//
// Fecha actualizacion:
//
// ejecuta el formulario para importar datos de un paciente a otro nuevo
function confirmImport(paciente_id, nombre, fecha) {
    var txt;
    var r = confirm("Seguro que desea importar los datos del paciente: "+ nombre + " con fecha de registro de "+ fecha+".");
    if (r == true) {
        $("#import_paciente_id").val(paciente_id);
        document.getElementById("form_import_data").submit();
    } else {
        txt = "You pressed Cancel!";
    }
}


// Autor: Vanessa Pineda Diaz
//
// Fecha creacion: 17-11-2015
//
// Autor actualizacion: Carlos Andres Torres Cruz
//
// Fecha actualizacion: 22-11-2015
//
//Implementación del data Table para mostrar los pacientes y traducir la página a español
//Nota: funciona para implementar cualquier tabla solo mandando el id y el nombre en singular y plural

function data_table( id, name, plural_name) {
    $(id).DataTable( {
        scrollX: true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ "+ plural_name,
            "zeroRecords": name + " no encontrad@",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Información no encontrada",
            "infoFiltered": "(filtrado desde _MAX_ registros)",
            "search":      "Buscar:",
            "paginate": {
                "next":       "Siguiente",
                "previous":   "Atrás"
            }
        }
    });
}

// Autor: Vanessa Pineda Diaz
//
// Fecha creacion: 17-11-2015
//
// Autor actualizacion: Carlos Andres Torres Cruz
//
// Fecha actualizacion: 29-11-2015
//
// Agrega dinámicamente campos de texto en el formulario de historia clínica en la sección
// de dolor
function add_more_dolor(){
    var dolor_count     = $('.dolor_count').length;
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    dolor_count++;
    $(wrapper).append("<div class='row text-center form_inline_patient'>" +
        "<div class='col-md-4 col-sm-4 col-xs-12'>" +
        "<div class='form-group'><label for='sistema_musculoesqueletico_dolors_attributes_"+dolor_count+"_localizacion'>Localización</label><input class='dolor_count form-control' type='text' name='sistema_musculoesqueletico[dolors_attributes]["+dolor_count+"][localizacion]' id='sistema_musculoesqueletico_dolors_attributes_"+dolor_count+"_localizacion'></div>"+
        "</div>"+
        "<div class='col-md-4 col-sm-4 col-xs-12'>" +
        "<div class='form-group'><label for='sistema_musculoesqueletico_dolors_attributes_"+dolor_count+"_tipo'>Tipo</label><input class='form-control' type='text' name='sistema_musculoesqueletico[dolors_attributes]["+dolor_count+"][tipo]' id='sistema_musculoesqueletico_dolors_attributes_"+dolor_count+"_tipo'></div>" +
        "</div>"+
        "<div class='col-md-4 col-sm-4 col-xs-12'>"+
        "<div class='form-group'><label for='sistema_musculoesqueletico_dolors_attributes_"+dolor_count+"_reposo'>Reposo</label><input class='form-control' type='text' name='sistema_musculoesqueletico[dolors_attributes]["+dolor_count+"][reposo]' id='sistema_musculoesqueletico_dolors_attributes_"+dolor_count+"_reposo'></div>"+
        "</div>"+
        "<div class='col-md-4 col-sm-4 col-xs-12'>"+
        "<div class='form-group'><label for='sistema_musculoesqueletico_dolors_attributes_"+dolor_count+"_movimiento'>Movimiento</label><input class='form-control' type='text' name='sistema_musculoesqueletico[dolors_attributes]["+dolor_count+"][movimiento]' id='sistema_musculoesqueletico_dolors_attributes_"+dolor_count+"_movimiento'></div>"+
        "</div>"+
        "<div class='col-md-4 col-sm-4 col-xs-12'>"+
        "<div class='form-group'><label for='sistema_musculoesqueletico_dolors_attributes_"+dolor_count+"_palpacion'>Palpación</label><input class='form-control' type='text' name='sistema_musculoesqueletico[dolors_attributes]["+dolor_count+"][palpacion]' id='sistema_musculoesqueletico_dolors_attributes_"+dolor_count+"_palpacion'></div>" +
        "</div>"+
        "<div class='col-md-4 col-sm-4 col-xs-12'>"+
        "<div class='form-group'><label for='sistema_musculoesqueletico_dolors_attributes_"+dolor_count+"_resistencia'>Resistencia</label><input class='form-control' type='text' name='sistema_musculoesqueletico[dolors_attributes]["+dolor_count+"][resistencia]' id='sistema_musculoesqueletico_dolors_attributes_"+dolor_count+"_resistencia'></div>" +
        "</div>"+
        "<div class='col-md-4 col-sm-4 col-xs-12'>"+
        "<div class='form-group'><label for='sistema_musculoesqueletico_dolors_attributes_"+dolor_count+"_exaservacion'>Exaservación</label><input class='form-control' type='text' name='sistema_musculoesqueletico[dolors_attributes]["+dolor_count+"][exaservacion]' id='sistema_musculoesqueletico_dolors_attributes_"+dolor_count+"_exaservacion'></div>" +
        "</div>" +

        "<a href=''#' class='remove_field link_remove'>Eliminar</a><hr></div>"
    );

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove();
    });
}

// Autor: Carlos Andres Torres Cruz
//
// Fecha creacion: 29-11-2015
//
// Autor actualizacion:
//
// Fecha actualizacion:
//
// Agrega dinámicamente campos de texto en el formulario de historia clínica en la sección
// de edema
function add_more_edema(){
    var dolor_count     = $('.edema_count').length;
    var wrapper         = $(".input_fields_wrap_edema"); //Fields wrapper
    dolor_count++;
    $(wrapper).append("<div class='row text-center form_inline_patien'>" +
        "<div class='col-md-4 col-sm-4 col-xs-12'>" +
            "<div class='form-group'><label for='sistema_musculoesqueletico_edemas_attributes_"+dolor_count+"_localizacion'>Localización </label><input class='edema_count form-control' type='text' name='sistema_musculoesqueletico[edemas_attributes]["+dolor_count+"][localizacion]' id='sistema_musculoesqueletico_edemas_attributes_"+dolor_count+"_localizacion'></div>"+
        "</div>" +
        "<div class='col-md-4 col-sm-4 col-xs-12'>" +
            "<div class='form-group'><label for='sistema_musculoesqueletico_edemas_attributes_"+dolor_count+"_derecho'>Derecho </label><input class='dolor_count form-control' type='text' name='sistema_musculoesqueletico[edemas_attributes]["+dolor_count+"][derecho]' id='sistema_musculoesqueletico_edemas_attributes_"+dolor_count+"_derecho'></div>" +
        "</div>"+
        "<div class='col-md-4 col-sm-4 col-xs-12'>" +
            "<div class='form-group'><label for='sistema_musculoesqueletico_edemas_attributes_"+dolor_count+"_izquierdo'>Izquierdo </label><input class='dolor_count form-control' type='text' name='sistema_musculoesqueletico[edemas_attributes]["+dolor_count+"][izquierdo]' id='sistema_musculoesqueletico_edemas_attributes_"+dolor_count+"_izquierdo'></div><br>" +
        "</div>" +
        "<div class='col-md-4 col-sm-4 col-xs-12'>" +
            "<div class='form-group'><label for='sistema_musculoesqueletico_edemas_attributes_"+dolor_count+"_total'>Total </label><input class='dolor_count form-control' type='text' name='sistema_musculoesqueletico[edemas_attributes]["+dolor_count+"][total]' id='sistema_musculoesqueletico_edemas_attributes_"+dolor_count+"_total'></div>" +
        "</div>"+
        "<div class='col-md-4 col-sm-4 col-xs-12'>" +
            "<div class='form-group'><label for='sistema_musculoesqueletico_edemas_attributes_"+dolor_count+"_grado'>Grado </label><input class='dolor_count form-control' type='text' name='sistema_musculoesqueletico[edemas_attributes]["+dolor_count+"][grado]' id='sistema_musculoesqueletico_edemas_attributes_"+dolor_count+"_grado'></div>" +
        "</div>" +

        "<a href=''#' class='remove_field link_remove'>Eliminar</a><hr></div>"
    );

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove();
    });
}

// Autor: Carlos Andres Torres Cruz
//
// Fecha creacion: 06-12-2015
//
// Autor actualizacion:
//
// Fecha actualizacion:
//
// Agrega dinámicamente campos de texto en el formulario de historia clínica en la sección
// de pruebas especiales
function add_more_tests(){
    var test_count     = $('.test_count').length;
    var wrapper         = $(".input_fields_wrap_tests"); //Fields wrapper
    test_count++;
    $.get( "/services/specials_tests", function( data ) {
        var select_string = "<select class='form-control chosen-selects' name='historia_clinica[pruebas_especiales_pacientes_attributes]["+test_count+"][pruebas_especial_id]' id='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_pruebas_especial_id'>";
        for( var i = 0; i < data.tests.length; i++){
            select_string = select_string + "<option value="+data.tests[i][0]+">"+data.tests[i][1]+"</option>";
        }
        select_string = select_string + "</select>";
        $(wrapper).append("<div class='row text-center'>" +
            "<div class=' col-md-4 col-sm-4 col-xs-12'>" +
                "<div class='form-group'><label for='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_pruebas_especial'>Pruebas especiales </label>"+
            select_string +
            "</div></div>" +
            "<div class=' col-md-4 col-sm-4 col-xs-12'>"+
                "<div class='form-group'><label for='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_zona'>Zona </label><input class='test_count form-control' type='text' name='historia_clinica[pruebas_especiales_pacientes_attributes]["+test_count+"][zona]' id='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_zona'></div>" +
            "</div>"  +
            "<div class=' col-md-4 col-sm-4 col-xs-12'>" +
                "<div class='form-group'><label for='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_derecho'>Derecho </label><input class='test_count form-control' type='text' name='historia_clinica[pruebas_especiales_pacientes_attributes]["+test_count+"][derecho]' id='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_derecho'></div>" +
            "</div>"+
            "<div class=' col-md-4 col-sm-4 col-xs-12'>" +
                "<div class='form-group'><label for='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_observaciones'>Observaciones </label><input class='test_count form-control' type='text' name='historia_clinica[pruebas_especiales_pacientes_attributes]["+test_count+"][observaciones]' id='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_observaciones'></div>" +
            "</div>" +
            "<div class=' col-md-4 col-sm-4 col-xs-12'>" +
                "<div class='form-group'><label for='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_izquierdo'>Izquierdo </label><input class='test_count form-control' type='text' name='historia_clinica[pruebas_especiales_pacientes_attributes]["+test_count+"][izquierdo]' id='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_izquierdo'></div>" +
            "</div>" +
            "<br><a href=''#' class='remove_field link_remove'>Eliminar</a><hr></div>"
        );

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove();
        });
        //$(".chosen-selects").chosen();
    }, "json" );
}

// Autor: Carlos Andres Torres Cruz
//
// Fecha creacion: 06-12-2015
//
// Autor actualizacion:
//
// Fecha actualizacion:
//
// Agrega dinámicamente campos de texto en el formulario de historia clínica en la sección
// de pruebas de flexibilidad
function add_more_flex(){
    var test_count     = $('.flex_count').length;
    var wrapper         = $(".input_fields_wrap_flex"); //Fields wrapper
    test_count++;

    $.get( "/services/flex_tests", function( data ) {
        //var select_string = "<select class='form-control chosen-selects' name='historia_clinica[pruebas_flexibilidad_pacientes_attributes]["+test_count+"][pruebas_flexibilidad_id]' id='historia_clinica_pruebas_flexibilidad_pacientes_attributes_"+test_count+"_pruebas_flexibilidad_id'>";
        var select_string ="";
        for( var i = 0; i < data.tests.length; i++){
           // select_string = select_string + "<option value="+data.tests[i][0]+">"+data.tests[i][1]+"</option>";
        }
        //select_string = select_string + "</select>";
        $(wrapper).append("<div class='row text-center'><div class='col-md-4 col-sm-4 col-xs-12'>"+
            select_string +
            "<div class='form-group'> <label for='historia_clinica_pruebas_flexibilidad_pacientes_attributes_"+test_count+"_musculo'>Músculo </label><input class='test_count form-control' type='text' name='historia_clinica[pruebas_flexibilidad_pacientes_attributes]["+test_count+"][musculo]' id='historia_clinica_pruebas_flexibilidad_pacientes_attributes_"+test_count+"_musculo'></div>" +
            "</div>"+
            "<div class='col-md-4 col-sm-4 col-xs-12'>"+
            "<div class='form-group'><label for='historia_clinica_pruebas_flexibilidad_pacientes_attributes_"+test_count+"_positivo'>Positivo </label><input class='test_count form-control' type='text' name='historia_clinica[pruebas_flexibilidad_pacientes_attributes]["+test_count+"][positivo]' id='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_positivo'></div>" +
            "</div>" +
            "<div class='col-md-4 col-sm-4 col-xs-12'>" +
            "<div class='form-group'><label for='historia_clinica_pruebas_flexibilidad_pacientes_attributes_"+test_count+"_negativo'>Negativo </label><input class='test_count form-control' type='text' name='historia_clinica[pruebas_flexibilidad_pacientes_attributes]["+test_count+"][negativo]' id='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_negativo'></div>" +
            "</div>" +
            "<div class='col-md-4 col-sm-4 col-xs-12'>" +
                "<div class='form-group'><label for='historia_clinica_pruebas_flexibilidad_pacientes_attributes_"+test_count+"_leve'>Leve </label><input class='test_count form-control' type='text' name='historia_clinica[pruebas_flexibilidad_pacientes_attributes]["+test_count+"][leve]' id='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_leve'></div>" +
            "</div>"+
            "<div class='col-md-4 col-sm-4 col-xs-12'>" +
            "<div class='form-group'><label for='historia_clinica_pruebas_flexibilidad_pacientes_attributes_"+test_count+"_moderado'>Moderado </label><input class='test_count form-control' type='text' name='historia_clinica[pruebas_flexibilidad_pacientes_attributes]["+test_count+"][moderado]' id='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_moderado'></div>" +
            "</div>"+
            "<div class='col-md-4 col-sm-4 col-xs-12'>" +
            "<div class='form-group'><label for='historia_clinica_pruebas_flexibilidad_pacientes_attributes_"+test_count+"_severo'>Severo </label><input class='test_count form-control' type='text' name='historia_clinica[pruebas_flexibilidad_pacientes_attributes]["+test_count+"][severo]' id='historia_clinica_pruebas_especiales_pacientes_attributes_"+test_count+"_severo'></div>" +
            "</div>"+
            "<a href=''#' class='remove_field link_remove'>Eliminar</a><hr></div>"
        );

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove();
        });
        //$(".chosen-selects").chosen();
    }, "json" );
}

// Autor: Carlos Andres Torres Cruz
//
// Fecha creacion: 15-12-2015
//
// Autor actualizacion:
//
// Fecha actualizacion:
//
// Agrega dinámicamente campos de texto en el formulario de historia clínica en la sección
// de sistema neuromuscular
function add_more_neuro(){
  var test_count     = $('.neuro_count').length;
  var wrapper         = $(".input_fields_wrap_neuro"); //Fields wrapper
  test_count++;

  $.get( "/services/neuro_test", function( data ) {
    var select_string = "<select name='historia_clinica[sistema_neuromusculars_attributes]["+test_count+"][prueba_neuromuscular_id]' id='historia_clinica_sistema_neuromusculars_attributes_"+test_count+"_prueba_neuromuscular_id', class='form-control chosen-selects'>";
    for( var i = 0; i < data.tests.length; i++){
      select_string = select_string + "<option value="+data.tests[i][0]+">"+data.tests[i][1]+"</option>";
    }
    select_string = select_string + "</select>";
    $(wrapper).append("<div class='row text-center'>" +
        "<div class='col-md-4 col-sm-4 col-xs-12'>" +
            "<div class='form-group'><label for='historia_clinica_pruebas_flexibilidad_pacientes_attributes_"+test_count+"_pruebas_especial'>Pruebas Neuromuscular </label>"+
      select_string +
      "</div></div>" +
      "<div class='col-md-4 col-sm-4 col-xs-12'>" +
        "<div class='form-group'><label for='historia_clinica_sistema_neuromusculars_attributes_"+test_count+"_localizacion'>Localización </label><input class='neuro_count form-control' type='text' name='historia_clinica[sistema_neuromusculars_attributes]["+test_count+"][localizacion]' id='historia_clinica_sistema_neuromusculars_attributes_"+test_count+"_localizacion'></div>" +
      "</div>" +
      "<div class='col-md-4 col-sm-4 col-xs-12'>" +
        "<div class='form-group'><label for='historia_clinica_sistema_neuromusculars_attributes_"+test_count+"_observacion'>Resultado </label><input class='form-control' type='text' name='historia_clinica[sistema_neuromusculars_attributes]["+test_count+"][resultado]' id='historia_clinica_sistema_neuromusculars_attributes_"+test_count+"_resultado'></div>" +
      "</div>" +
      "<div class='col-md-4 col-sm-4 col-xs-12'>" +
        "<div class='form-group'><label for='historia_clinica_sistema_neuromusculars_attributes_"+test_count+"_observacion'>Observación </label><textarea class='form-control' name='historia_clinica[sistema_neuromusculars_attributes]["+test_count+"][observacion]' id='historia_clinica_sistema_neuromusculars_attributes_"+test_count+"_observacion'></textarea></div>" +
      "</div>" +
      "<a href=''#' class='remove_field link_remove'>Eliminar</a><hr></div>"
    );

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
      e.preventDefault(); $(this).parent('div').remove();
    });
    //$(".chosen-selects").chosen();
  }, "json" );
}