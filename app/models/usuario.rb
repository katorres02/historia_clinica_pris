class Usuario < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  mount_base64_uploader :logo_empresa, UserImageUploader
  mount_uploader :firma_digital, UserImageUploader

  has_many :pacientes

  has_many :user_services
  has_many :services, through: :user_services

  accepts_nested_attributes_for :user_services, reject_if: :all_blank,  allow_destroy: true
end
