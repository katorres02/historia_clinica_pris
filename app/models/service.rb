class Service < ActiveRecord::Base
  has_many :user_services
  has_many :usuarios, through: :user_services
end
