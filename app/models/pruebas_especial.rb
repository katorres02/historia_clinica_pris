class PruebasEspecial < ActiveRecord::Base
  has_many :pruebas_especiales_pacientes
  has_many :historia_clinicas, through: :pruebas_especiales_pacientes
end
