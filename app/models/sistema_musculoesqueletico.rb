class SistemaMusculoesqueletico < ActiveRecord::Base
  belongs_to :historia_clinica
  has_one :perimetro, dependent: :destroy
  has_one :adipometrium, dependent: :destroy
  has_one :cuello, dependent: :destroy
  has_one :hombro, dependent: :destroy
  has_one :musculos_superior, dependent: :destroy
  has_one :tronco, dependent: :destroy
  has_one :cadera, dependent: :destroy
  has_one :musculos_inferior, dependent: :destroy
  has_one :postura, dependent: :destroy
  has_one :fuerza_muscular, dependent: :destroy
  has_many :dolors, dependent: :destroy
  has_many :edemas, dependent: :destroy
  has_many :daniels, dependent: :destroy

  accepts_nested_attributes_for :perimetro, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :adipometrium, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :cuello, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :hombro, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :musculos_superior, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :tronco, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :cadera, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :musculos_inferior, allow_destroy: true, reject_if: :all_blank

  # la sentencia reject_id: sirve para ignorar los registros donde todos los elementos esten vacios
  # esto puede pasar cuando el usuario presiona el agregar campos varias veces y envia el formulario
  # con esos campos vacios
  accepts_nested_attributes_for :dolors, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :edemas, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :daniels, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :postura, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :fuerza_muscular, reject_if: :all_blank,  allow_destroy: true
end
