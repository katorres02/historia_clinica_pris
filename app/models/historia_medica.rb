class HistoriaMedica < ActiveRecord::Base
  belongs_to :paciente

  has_many :impresion_diagnosticas, dependent: :destroy

  validates :motivo_consulta, presence: true

  accepts_nested_attributes_for :impresion_diagnosticas, reject_if: :all_blank,  allow_destroy: true
end
