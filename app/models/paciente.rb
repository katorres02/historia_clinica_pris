class Paciente < ActiveRecord::Base
  belongs_to :usuario
  belongs_to :estado_civil
  belongs_to :type_document

  has_many :evolucions, dependent: :destroy
  has_many :historia_clinicas, dependent: :destroy
  has_many :historia_medicas, dependent: :destroy

  #validates :genero, presence: true

  # busqueda predictiva de pacientes
  def self.search_filter(param)
    param = param.parameterize.underscore.humanize.downcase
    where("LOWER(pacientes.documento) ILIKE('%#{param}%') "\
    "OR LOWER(pacientes.email) ILIKE('%#{param}%') "\
    "OR LOWER(pacientes.nombre) ILIKE('%#{param}%')")
  end

  # búsqueda de paciente por documento o email
  def self.search_by_term(term)
    joins('LEFT JOIN memberships ON pacientes.membership_id = memberships.id')
    .where("pacientes.documento ILIKE('%#{term}%') "\
    "OR LOWER(pacientes.email) ILIKE('%#{term.downcase}%') "\
    "OR LOWER(pacientes.nombre) ILIKE('%#{term.downcase}%') ")
    .select('pacientes.id, pacientes.nombre, pacientes.email, pacientes.membership_id, memberships.name as membership_name, memberships.price as membership_price')
  end
end
