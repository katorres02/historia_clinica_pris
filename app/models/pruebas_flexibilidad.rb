class PruebasFlexibilidad < ActiveRecord::Base
  has_many :pruebas_flexibilidad_pacientes
  has_many :historia_clinicas, through: :pruebas_flexibilidad_pacientes
end
