class SistemaNeuromuscular < ActiveRecord::Base
  belongs_to :historia_clinica
  belongs_to :prueba_neuromuscular

  has_one :transicion, dependent: :destroy
  has_one :sistema_neurosensorial, dependent: :destroy
  has_one :memorium, dependent: :destroy
  has_one :psicomotricidad, dependent: :destroy
  has_one :coordinacion_motora, dependent: :destroy
  has_one :actividad, dependent: :destroy
  has_one :clasificacion_internacional, dependent: :destroy

=begin
  
  # estas relaciones, se eliminaron y se enviaron a la historia clinica 29-01-2018

  has_one :transicion, dependent: :destroy
  has_one :sistema_neurosensorial, dependent: :destroy
  has_one :memorium, dependent: :destroy
  has_one :psicomotricidad, dependent: :destroy
  has_one :coordinacion_motora, dependent: :destroy
  has_one :actividad, dependent: :destroy
  has_one :clasificacion_internacional, dependent: :destroy

  accepts_nested_attributes_for :transicion, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :sistema_neurosensorial, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :memorium, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :psicomotricidad, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :coordinacion_motora, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :actividad, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :clasificacion_internacional, reject_if: :all_blank,  allow_destroy: true
=end
end
