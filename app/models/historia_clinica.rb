class HistoriaClinica < ActiveRecord::Base
  belongs_to :paciente
  has_one :sistema_cardiopulmonar, dependent: :destroy
  has_one :sistema_musculoesqueletico, dependent: :destroy
  has_one :sistema_tegumentario, dependent: :destroy
  has_one :sistema_genitourinario, dependent: :destroy
  has_one :sistema_neuromuscular, dependent: :destroy

  has_many :pruebas_especiales_pacientes, dependent: :destroy
  has_many :pruebas_especials, through: :pruebas_especiales_pacientes

  has_many :pruebas_flexibilidad_pacientes, dependent: :destroy
  has_many :pruebas_flexibilidads, through: :pruebas_flexibilidad_pacientes

  has_one :diagnostico, dependent: :destroy

  has_one :prenatale, dependent: :destroy
  has_one :perinatale, dependent: :destroy
  has_one :postnatale, dependent: :destroy
  has_one :desarrollo_motor, dependent: :destroy

  accepts_nested_attributes_for :pruebas_especiales_pacientes, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :pruebas_flexibilidad_pacientes, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :sistema_neuromuscular, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :sistema_tegumentario, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :diagnostico, reject_if: :all_blank,  allow_destroy: true

  accepts_nested_attributes_for :prenatale, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :perinatale, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :postnatale, reject_if: :all_blank,  allow_destroy: true

  has_one :transicion, dependent: :destroy
  has_one :sistema_neurosensorial, dependent: :destroy
  has_one :memorium, dependent: :destroy
  has_one :psicomotricidad, dependent: :destroy
  has_one :coordinacion_motora, dependent: :destroy
  has_one :actividad, dependent: :destroy
  has_one :clasificacion_internacional, dependent: :destroy

  accepts_nested_attributes_for :transicion, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :sistema_neurosensorial, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :memorium, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :psicomotricidad, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :coordinacion_motora, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :actividad, reject_if: :all_blank,  allow_destroy: true
  accepts_nested_attributes_for :clasificacion_internacional, reject_if: :all_blank,  allow_destroy: true

  #validates :antecedentes_familiares, presence: true
end
