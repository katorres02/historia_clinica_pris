module MusculoesqueleticoHelper

  #Validación de perímetro para la impresión
  def validate_perimetro_impression(model)
    return true if model.nil?
    if model.pecho.nil? && model.abdomen.nil? && model.biceps.nil? && model.cadera.nil? && model.muslo.nil? && model.pierna.nil?
      true
    else
      false
    end
  end

  #Validación de simetría para la impresión
  def validate_simetria_impression(model)
    return true if model.nil?
    if model.m_superior_r_derecho.nil? && model.m_superior_r_izquierdo.nil? && model.m_superior_a_derecho.nil? && model.m_superior_a_izquierdo.nil? && model.m_superiores_diferencia.nil? && model.m_inferior_r_derecho.nil? && model.m_inferior_r_izquierdo.nil? && model.m_inferior_a_derecho.nil? && model.m_inferior_a_izquierdo.nil? && model.m_inferiores_diferencia.nil?
      true
    else
      false
    end
  end

  #Validación de adipometría para la impresión
  def validate_adipometria_impression(model)
    return true if model.nil?
    if model.triceps.nil? && model.s_escap.nil? && model.s_iliaco.nil? && model.abdomen.nil? && model.muslo.nil? && model.pierna.nil?
      true
    else
      false
    end
  end

  #Validación de cuello para rangos de movilidad articular para la impresión
  def validate_cuello_impression(model)
    return true if model.nil?
    if model.flexion_derecho.blank? && model.ext_derecho.blank? && model.incl_lateral_der.blank? && model.rotacion_lat_der.blank? && model.flexion_izquierdo.blank? && model.ext_izquierdo.blank? && model.incl_lateral_izq.blank? && model.rotacion_lat_izq.blank?
      true
    else
      false
    end
  end

  #Validación de Hombro para rangos de movilidad articular para la impresión
  def validate_hombro_impression(model)
    return true if model.nil?
    if model.flexion_derecho.blank? && model.ext_derecho.blank? && model.abduccion_der.blank? && model.aduccion_der.blank? && model.rotacion_interna_der.blank? && model.rotacion_externa_der.blank? && model.flexion_izquierdo.blank? && model.ext_izquierdo.blank? && model.abduccion_izq.blank? && model.aduccion_izq.blank? && model.rotacion_interna_izq.blank? && model.rotacion_externa_izq.blank?
      true
    else
      false
    end
  end

  #Validación de Miembros superiores para rangos de movilidad articular para la impresión
  def validate_msuperiores_impression(model)
    return true if model.nil?
    if model.flexion_codo_der.blank? && model.extension_codo_der.blank? && model.supinacion_codo_der.blank? && model.pronacion_codo_der.blank? && model.extension_muneca_der.blank? && model.desviacion_ulnar_muneca_der.blank? && model.desviacion_radial_muneca_der.blank? && model.flexion_codo_izq.blank? && model.extension_codo_izq.blank? && model.supinacion_codo_izq.blank? && model.pronacion_codo_izq.blank? && model.extension_muneca_izq.blank? && model.desviacion_ulnar_muneca_izq.blank? && model.desviacion_radial_muneca_izq.blank?
      true
    else
      false
    end
  end

  #Validación de Tronco para rangos de movilidad articular para la impresión
  def validate_tronco_impression(model)
    return true if model.nil?
    if model.flexion_der.blank? && model.extension_der.blank? && model.flexion_lateral_der.blank? && model.rotacion_der.blank? && model.flexion_izq.blank? && model.extension_izq.blank? && model.flexion_lateral_izq.blank? && model.rotacion_izq.blank?
      true
    else
      false
    end
  end

  #Validación de Cadera para rangos de movilidad articular para la impresión
  def validate_cadera_impression(model)
    return true if model.nil?
    if model.flexion_der.blank? && model.extension_der.blank? && model.abduccion_der.blank? && model.aduccion_der.blank? && model.rotacion_medial_der.blank? && model.rotacion_lateral_der.blank? && model.flexion_izq.blank? && model.extension_izq.blank? && model.abduccion_izq.blank? && model.aduccion_izq.blank? && model.rotacion_medial_izq.blank? && model.rotacion_lateral_izq.blank?
      true
    else
      false
    end
  end

  #Validación de Miembros inferiores para rangos de movilidad articular para la impresión
  def validate_minferiores_impression(model)
    return true if model.nil?
    if model.flexion_rodilla_der.blank? && model.extension_der.blank? && model.dorsflexion_tobillo_der.blank? && model.plantiflexion_der.blank? && model.inversion_tobillo_der.blank? && model.eversion_tobillo_der.blank? && model.flexion_rodilla_izq.blank? && model.extension_izq.blank? && model.dorsflexion_tobillo_izq.blank? && model.plantiflexion_izq.blank? && model.inversion_tobillo_izq.blank? && model.eversion_tobillo_izq.blank?
      true
    else
      false
    end
  end

  #Validación de rangos de movilidad
  def validate_rangos_movilidad_impression(model)
    return true if model.nil?
    !validate_cuello_impression(model.cuello) || !validate_hombro_impression(model.hombro) || !validate_tronco_impression(model.tronco) || !validate_minferiores_impression(model.musculos_inferior) ||  !validate_cadera_impression(model.cadera) || !validate_msuperiores_impression(model.musculos_superior)
  end

  #validación de pruebas especiales
  def validate_pruebas_especiales(model)
    return true if model.nil?
    if model[0].zona.blank? && model[0].derecho.blank? && model[0].izquierdo.blank? && model[0].observaciones.blank?
      true
    else
      false
    end
  end

  #validación de pruebas flexibilidad
  def validate_pruebas_flexibilidad(model)
    return true if model.nil?
    if model[0].musculo.blank? && model[0].positivo.blank? && model[0].negativo.blank? && model[0].leve.blank? && model[0].moderado.blank? && model[0].severo.blank?
      true
    else
      false
    end
  end


end
