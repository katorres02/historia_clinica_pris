module ApplicationHelper
  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 16-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Metodo: estos metodos se usan para que los formularios de devise
  # detecten el recurso a usar y no generen
  # error al redireccionar a donde deben ir

  def resource_name
    :usuario
  end

  def resource_class
    Usuario
  end

  def resource
    @resource ||= Usuario.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:usuario]
  end
end
