class ApplicationMailer < ActionMailer::Base
  default from: "pruebas.desarrollador1@gmail.com"
  layout 'mailer'
end
