class InvoiceMailer < ApplicationMailer
  default from: "pruebas.desarrollador1@gmail.com"

  def new_payment(invoice, paciente, company, pdf)
    @invoice = invoice
    @user    = paciente
    @company = company
    attachments["#{@company.name.parameterize} Factura ##{@invoice.invoice_number}.pdf"] = pdf

    mail(to: @user.email, subject: "#{@company.name}. Factura generada ##{@invoice.invoice_number}")
  end
end
