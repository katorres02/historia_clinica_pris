module Fisio
  class RemisionExternaController < FisioController
    before_action :authenticate_usuario!
    before_action :set_patient
    before_action :set_history
    before_action :set_remi, except: [:new, :create]

    def new
      @remi = @history.build_diagnostico
    end

    def edit
    end

    def create
      @remi =  Diagnostico.new(diagnostico_params)
      if @remi.save
        flash[:notice] = 'Registro creado'
        redirect_to edit_fisio_patient_clinic_history_remision_externa_path(@patient, @history, @remi)
      else
        render :new
      end
    end

    def update
      if @remi.update(diagnostico_params)
        flash[:notice] = 'Registro editado'
        redirect_to edit_fisio_patient_clinic_history_remision_externa_path(@patient, @history, @remi)
      else
        render :edit
      end
    end

    private

    # setear paciente atual
    def set_patient
      @patient = current_usuario.pacientes.find(params[:patient_id])
    end

    # setear historia clinica
    def set_history
      @history = HistoriaClinica.find(params[:clinic_history_id])
    end

    # setea sistema cardiopulmonar
    def set_remi
      if params[:id].nil?
        @remi =  @history.diagnostico 
      else
        @remi = Diagnostico.find(params[:id])
      end
    end

    # parámetros fuertes
    def diagnostico_params
      params.require(:diagnostico).permit(:id, :historia_clinica_id, :diagnostico,
        :recomendacion, :restriccion, :sugerencia_terapeutica, :objetivo_general, :objetivo_especifico)
    end
  end
end

