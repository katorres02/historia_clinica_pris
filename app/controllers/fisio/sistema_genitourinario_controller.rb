module Fisio
  class SistemaGenitourinarioController < FisioController
    before_action :authenticate_usuario!
    before_action :set_patient
    before_action :set_history
    before_action :set_genito, except: [:new, :create]

    def new
      @genito = @history.build_sistema_genitourinario
    end

    def edit
    end

    def create
      @genito =  SistemaGenitourinario.new(genito_params)
      if @genito.save
        flash[:notice] = 'Registro creado'
        redirect_to edit_fisio_patient_clinic_history_sistema_genitourinario_path(@patient, @history, @genito)
      else
        render :new
      end
    end

    def update
      if @genito.update(genito_params)
        flash[:notice] = 'Registro editado'
        redirect_to edit_fisio_patient_clinic_history_sistema_genitourinario_path(@patient, @history, @genito)
      else
        render :edit
      end
    end

    private

    # setear paciente atual
    def set_patient
      @patient = current_usuario.pacientes.find(params[:patient_id])
    end

    # setear historia clinica
    def set_history
      @history = HistoriaClinica.find(params[:clinic_history_id])
    end

    # setea sistema genitourinario
    def set_genito
      @genito = SistemaGenitourinario.find(params[:id])
    end

    # parámetros fuertes
    def genito_params
      params.require(:sistema_genitourinario).permit(:id, :historia_clinica_id, :indemne, :incontinencia,
        :prolapsos, :diastasis, :trastornos, :incontinencia_fecal, :estrenimiento, :disfuncion_sexual, :observaciones)
    end
  end
end

