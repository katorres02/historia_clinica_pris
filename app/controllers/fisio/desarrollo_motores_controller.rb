module Fisio
  class DesarrolloMotoresController < FisioController
    before_action :authenticate_usuario!
    before_action :set_patient
    before_action :set_history
    before_action :set_motore, except: [:new, :create]

    def new
      @motores = @history.build_desarrollo_motor
    end

    def edit
    end

    def create
      @motores =  DesarrolloMotor.new(motore_params)
      if @motores.save
        flash[:notice] = 'Registro creado'
        redirect_to edit_fisio_patient_clinic_history_desarrollo_motore_path(@patient, @history, @motores)
      else
        render :new
      end
    end

    def update
      if @motores.update(motore_params)
        flash[:notice] = 'Registro editado'
        redirect_to edit_fisio_patient_clinic_history_desarrollo_motore_path(@patient, @history, @motores)
      else
        render :edit
      end
    end

    private

    # setear paciente atual
    def set_patient
      @patient = current_usuario.pacientes.find(params[:patient_id])
    end

    # setear historia clinica
    def set_history
      @history = HistoriaClinica.find(params[:clinic_history_id])
    end

    def set_motore
      if params[:id].nil?
        @motores =  @history.desarrollo_motor 
      else
        @motores = DesarrolloMotor.find(params[:id])
      end
    end

    def motore_params
      params.require(:desarrollo_motor).permit(:id, :historia_clinica_id, :centro_cefalico,
        :sedente, :rolado, :gateo, :bipedo, :marcha, :adicion, :antecedentes_psicosociales,
        :nucleo_familiar, :escolaridad, :habitos, :observacion)
    end
  end
end

