module Fisio
  class SistemaNeuromuscularController < FisioController
    before_action :authenticate_usuario!
    before_action :set_patient
    before_action :set_history
    before_action :set_neuro, except: [:new, :create, :update_extra]

    def new
      @neuro = @history.build_sistema_neuromuscular

      @history.build_transicion
      @history.build_sistema_neurosensorial
      @history.build_memorium
      @history.build_psicomotricidad
      @history.build_coordinacion_motora
      @history.build_actividad
      @history.build_clasificacion_internacional
    end

    def edit
      @neuro = @history.build_sistema_neuromuscular if @history.sistema_neuromuscular.nil?

      @history.build_transicion if @history.transicion.nil?
      @history.build_sistema_neurosensorial if @history.sistema_neurosensorial.nil?
      @history.build_memorium if @history.memorium.nil?
      @history.build_psicomotricidad if @history.psicomotricidad.nil?
      @history.build_coordinacion_motora if @history.coordinacion_motora.nil?
      @history.build_actividad if @history.actividad.nil?
      @history.build_clasificacion_internacional if @history.clasificacion_internacional.nil?
    end

    def create
      @neuro = SistemaNeuromuscular.new(neuro_params)
      if @neuro.save
        flash[:notice] = 'Registro editado'
        redirect_to edit_fisio_patient_clinic_history_sistema_neuromuscular_path(@patient, @history, @neuro)
      else
        render :edit
      end
    end

    def update
      if @neuro.update(neuro_params)
        flash[:notice] = 'Registro editado'
        redirect_to edit_fisio_patient_clinic_history_sistema_neuromuscular_path(@patient, @history, @neuro)
      else
        render :edit
      end
    end

    def update_extra
      if params[:neuro_id].empty?
        @neuro = @history.build_sistema_neuromuscular
      else
        @neuro = SistemaNeuromuscular.find(params[:neuro_id])
      end
      if @history.update(history_params)
        flash[:notice] = 'Registro editado'
        redirect_to edit_fisio_patient_clinic_history_sistema_neuromuscular_path(@patient, @history, @neuro)
      else
        render :edit
      end
    end

    private

    # setear paciente atual
    def set_patient
      @patient = current_usuario.pacientes.find(params[:patient_id])
    end

    # setear historia clinica
    def set_history
      @history = HistoriaClinica.find(params[:clinic_history_id])
    end

    def set_neuro
      @neuro = SistemaNeuromuscular.find(params[:id])
    end

    # parametros fuertes
    def neuro_params
      params.require(:sistema_neuromuscular).permit(:id, :historia_clinica_id,
      :marcha, :propiocepcion, :equilibrio, :tono_muscular,
      :prueba_neuromuscular_id, :localizacion, :resultado, :observacion,
      :estado_mental_espacio, :estado_mental_tiempo, :estado_mental_persona,
        transicion_attributes: [:id, :decubito_prono, :supino, :decubito_lateral, :sedente_bipedo],
        sistema_neurosensorial_attributes: [:id, :vestirse, :disfruta_banio, :cepilla, :molesta_ensucia,
          :molesta_sonido, :busca_sonido, :responde_llamado, :disfruta_estimulo, :persecucion_ocular,
          :superficie_inestable, :juegos_mecanicos, :juegos_aparatos],
        memorium_attributes: [:id, :anterograda, :retrogada, :queja_memoria],
        psicomotricidad_attributes: [:id, :lanzamiento, :pateo, :recepcion, :velocidad, :tamborilear,
          :escritura, :salto_bipodal, :salto_unipodal, :ascenso_descenso, :pedaleo,
          :trepado, :carrera, :dominancia, :lateralidad, :nocion_cuerpo],
        coordinacion_motora_attributes: [:id, :mano_boca, :mano_pie, :linea_media, :toma_objeto, :toma_mas,
          :prueba_diadococinesias, :prueba_dedo_nariz, :observaciones],
        actividad_attributes: [:id, :vestirse, :tomar_banio, :levantar_sentar, :subir_bajar_escalera,
          :subir_bajar_rampa, :caminar_barrio, :preparar_comida, :limpiar_casa, :responder_telefono],
        clasificacion_internacional_attributes: [:id, :deficiencia_principal, :limitacion_actividades,
          :factores_ambientales, :observacion])
    end

    def history_params
      params.require(:historia_clinica).permit(:id,
        transicion_attributes: [:id, :decubito_prono, :supino, :decubito_lateral, :sedente_bipedo],
        sistema_neurosensorial_attributes: [:id, :vestirse, :disfruta_banio, :cepilla, :molesta_ensucia,
          :molesta_sonido, :busca_sonido, :responde_llamado, :disfruta_estimulo, :persecucion_ocular,
          :superficie_inestable, :juegos_mecanicos, :juegos_aparatos],
        memorium_attributes: [:id, :anterograda, :retrogada, :queja_memoria],
        psicomotricidad_attributes: [:id, :lanzamiento, :pateo, :recepcion, :velocidad, :tamborilear,
          :escritura, :salto_bipodal, :salto_unipodal, :ascenso_descenso, :pedaleo,
          :trepado, :carrera, :dominancia, :lateralidad, :nocion_cuerpo],
        coordinacion_motora_attributes: [:id, :mano_boca, :mano_pie, :linea_media, :toma_objeto, :toma_mas,
          :prueba_diadococinesias, :prueba_dedo_nariz, :observaciones],
        actividad_attributes: [:id, :vestirse, :tomar_banio, :levantar_sentar, :subir_bajar_escalera,
          :subir_bajar_rampa, :caminar_barrio, :preparar_comida, :limpiar_casa, :responder_telefono],
        clasificacion_internacional_attributes: [:id, :deficiencia_principal, :limitacion_actividades,
          :factores_ambientales, :observacion])
    end
  end
end
