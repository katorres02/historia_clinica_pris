module Fisio
  class PrintController < FisioController
    before_action :authenticate_usuario!
    before_action :set_patient
    before_action :set_history
    
    def single
      respond_to do |format|
        format.html
        format.pdf do
          render template: "fisio/clinic_histories/print", pdf: "#{@patient.nombre}-Historia Clinica-#{@history.created_at.strftime('%d-%m-%Y')}",
          encoding: 'utf-8', :margin => {:bottom => 25}, footer:  { html: {   template:'shared/pdf_footer' } }
        end
      end
    end

    private

    # setear paciente atual
    def set_patient
      @patient = current_usuario.pacientes.find(params[:patient_id])
    end

    # setear historia clinica
    def set_history
      @history = HistoriaClinica.find(params[:clinic_history_id])
    end
  end
end

