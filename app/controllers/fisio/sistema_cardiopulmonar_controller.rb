module Fisio
  class SistemaCardiopulmonarController < FisioController
    before_action :authenticate_usuario!
    before_action :set_patient
    before_action :set_history
    before_action :set_cardio, except: [:new, :create]

    def new
      @cardio = @history.build_sistema_cardiopulmonar
    end

    def edit
    end

    def create
      @cardio =  SistemaCardiopulmonar.new(cardio_params)
      if @cardio.save
        flash[:notice] = 'Registro creado'
        redirect_to edit_fisio_patient_clinic_history_sistema_cardiopulmonar_path(@patient, @history, @cardio)
      else
        render :new
      end
    end

    def update
      if @cardio.update(cardio_params)
        flash[:notice] = 'Registro editado'
        redirect_to edit_fisio_patient_clinic_history_sistema_cardiopulmonar_path(@patient, @history, @cardio)
      else
        render :edit
      end
    end

    private

    # setear paciente atual
    def set_patient
      @patient = current_usuario.pacientes.find(params[:patient_id])
    end

    # setear historia clinica
    def set_history
      @history = HistoriaClinica.find(params[:clinic_history_id])
    end

    # setea sistema cardiopulmonar
    def set_cardio
      if params[:id].nil?
        @cardio =  @history.sistema_cardiopulmonar 
      else
        @cardio = SistemaCardiopulmonar.find(params[:id])
      end
    end

    # parámetros fuertes
    def cardio_params
      params.require(:sistema_cardiopulmonar).permit(:id, :historia_clinica_id, :pa,
        :fc, :fr, :sao2, :ausc_pulmonar, :ausc_cardiaca)
    end
  end
end

