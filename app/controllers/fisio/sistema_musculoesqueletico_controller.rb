module Fisio
  class SistemaMusculoesqueleticoController < FisioController
    before_action :authenticate_usuario!
    before_action :set_patient
    before_action :set_history
    before_action :set_musculo, except: [:new, :create]

    def new
      @musculo = @history.build_sistema_musculoesqueletico
      @musculo.build_perimetro
      @musculo.build_adipometrium
      @musculo.build_cuello
      @musculo.build_hombro
      @musculo.build_musculos_superior
      @musculo.build_tronco
      @musculo.build_cadera
      @musculo.build_musculos_inferior
      @musculo.build_postura
      @musculo.build_fuerza_muscular
      @musculo.dolors.build
      @dolor_builds = @musculo.dolors.count #cantidad de builds que tiene el formulario
      @musculo.edemas.build
      @edema_builds = @musculo.edemas.count #cantidad de builds que tiene el formulario
      @musculo.daniels.build
    end

    def edit
      @musculo.build_fuerza_muscular if @musculo.fuerza_muscular.nil?
      @musculo.build_postura if @musculo.postura.nil?
      @dolor_builds = @musculo.dolors.count #cantidad de builds que tiene el formulario
      @musculo.dolors.build if @musculo.dolors.count == 0
      @musculo.edemas.build if @musculo.edemas.count == 0
      @edema_builds = @musculo.edemas.count #cantidad de builds que tiene el formulario
      @musculo.daniels.build if @musculo.daniels.count == 0

      @musculo.build_cuello if @musculo.cuello.nil?
      @musculo.build_hombro if @musculo.hombro.nil?
      @musculo.build_musculos_superior if @musculo.musculos_superior.nil?
      @musculo.build_tronco if @musculo.tronco.nil?
      @musculo.build_cadera if @musculo.cadera.nil?
      @musculo.build_musculos_inferior if @musculo.musculos_inferior.nil?
    end

    def create
      @musculo =  SistemaMusculoesqueletico.new(musculo_params)
      if @musculo.save
        flash[:notice] = 'Registro creado'
        redirect_to edit_fisio_patient_clinic_history_sistema_musculoesqueletico_path(@patient, @history, @musculo)
      else
        render :new
      end
    end

    def update
      if @musculo.update(musculo_params)
        flash[:notice] = 'Registro editado'
        redirect_to edit_fisio_patient_clinic_history_sistema_musculoesqueletico_path(@patient, @history, @musculo)
      else
        render :edit
      end
    end

    private

    # setear paciente atual
    def set_patient
      @patient = current_usuario.pacientes.find(params[:patient_id])
    end

    # setear historia clinica
    def set_history
      @history = HistoriaClinica.find(params[:clinic_history_id])
    end

    # setea sistema cardiopulmonar
    def set_musculo
      if params[:id].nil?
        @musculo =  @history.sistema_musculoesqueletico 
      else
        @musculo = SistemaMusculoesqueletico.find(params[:id])
      end
    end

    # parámetros fuertes
    def musculo_params
      params.require(:sistema_musculoesqueletico).permit(:id, :historia_clinica_id, :talla, :peso, :imc, :grasa_total, :grasa_localizada,
        :m_superior_r_derecho, :m_superior_r_izquierdo, :m_superior_a_derecho, :m_superior_a_izquierdo, :m_superiores_diferencia,
        :m_inferior_r_derecho, :m_inferior_r_izquierdo, :m_inferior_a_derecho, :m_inferior_a_izquierdo, :m_inferiores_diferencia,
        :unidad_talla,
          perimetro_attributes: [:id, :biceps, :pecho, :abdomen, :cadera, :muslo, :pierna,
                                :sistema_musculoesqueletico_id],
          adipometrium_attributes: [:id, :triceps, :s_escap, :s_iliaco, :abdomen, :muslo,
                                   :pierna],
          cuello_attributes: [:id, :flexion_derecho, :flexion_izquierdo, :ext_derecho,
                              :ext_izquierdo, :incl_lateral_der, :incl_lateral_izq,
                              :rotacion_lat_der, :rotacion_lat_izq],
          hombro_attributes: [:id, :flexion_derecho, :flexion_izquierdo, :ext_derecho,
                              :ext_izquierdo, :abduccion_der, :abduccion_izq, :aduccion_der,
                              :aduccion_izq, :rotacion_interna_der, :rotacion_interna_izq,
                              :rotacion_externa_der, :rotacion_externa_izq],
          musculos_superior_attributes: [:id, :flexion_codo_der, :flexion_codo_izq, :extension_codo_der,
                              :extension_codo_izq, :supinacion_codo_der, :supinacion_codo_izq,
                              :pronacion_codo_der, :pronacion_codo_izq, :extension_muneca_der,
                              :extension_muneca_izq, :desviacion_ulnar_muneca_der, :desviacion_ulnar_muneca_izq,
                              :desviacion_radial_muneca_der, :desviacion_radial_muneca_izq],
          tronco_attributes: [:id, :flexion_der, :flexion_izq, :extension_der, :extension_izq,
                              :flexion_lateral_der, :flexion_lateral_izq, :rotacion_der, :rotacion_izq],
          cadera_attributes: [:id, :flexion_der, :flexion_izq, :extension_der, :extension_izq, :abduccion_der,
                              :abduccion_izq, :aduccion_der, :aduccion_izq, :rotacion_medial_der,
                              :rotacion_medial_izq, :rotacion_lateral_der, :rotacion_lateral_izq],
          musculos_inferior_attributes: [:id, :flexion_rodilla_der, :flexion_rodilla_izq, :extension_der,
                                         :extension_izq, :dorsflexion_tobillo_der, :dorsflexion_tobillo_izq,
                                         :plantiflexion_der, :plantiflexion_izq, :inversion_tobillo_der,
                                          :inversion_tobillo_izq, :eversion_tobillo_der, :eversion_tobillo_izq],
          dolors_attributes: [:id, :localizacion, :tipo, :reposo, :palpacion, :movimiento, :resistencia,
                              :exaservacion, :_destroy],
          edemas_attributes: [:id, :localizacion, :derecho, :izquierdo, :total, :grado, :_destroy],
          daniels_attributes: [:id, :musculo_cero, :musculo_uno, :musculo_dos, :musculo_tres, :musculo_cuatro, :musculo_cinco, :_destroy],
          postura_attributes: [:id, :cabeza_cuello, :hombros, :tronco, :miembros_superiores, :miembros_inferiores,
            :columna, :pelvis, :observacion, :_destroy],
          fuerza_muscular_attributes: [:id, :cuello_i, :cuello_d, :hombro_i, :hombro_d, :codo_i, :codo_d,
            :muneca_i, :muneca_d, :tronco_i, :tronco_d, :miembro_superior_i, :miembro_superior_d,
            :miembro_inferior_i, :miembro_inferior_d, :ayudas, :ayudas_text, :_destroy])
    end
  end
end

