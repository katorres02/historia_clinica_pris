module Fisio
  class ClinicHistoriesController < FisioController
    before_action :authenticate_usuario!
    before_action :set_patient
    before_action :set_history, only: [:edit, :update]

    def index
      @histories = @patient.historia_clinicas.order('created_at desc')
      .paginate(page: params[:page], per_page: 15)
    end

    def new
      @history = @patient.historia_clinicas.new
      @history.build_prenatale
      @history.build_perinatale
      @history.build_postnatale
    end

    def edit
      @history.build_prenatale if @history.prenatale.nil?
      @history.build_perinatale if @history.perinatale.nil?
      @history.build_postnatale if @history.postnatale.nil?
    end

    def create
      @history = HistoriaClinica.new(history_params)
      if @history.save
        flash[:notice] = "Historia clínica creada"
        redirect_to edit_fisio_patient_clinic_history_path(@patient, @history)
      else
        render :new
      end
    end

    def update
      if @history.update(history_params)
        flash[:notice] = "Información actualizada"
        redirect_to edit_fisio_patient_clinic_history_path(@patient, @history)
      else
        render :edit
      end
    end

    private

    # setear paciente atual
    def set_patient
      @patient = current_usuario.pacientes.find(params[:patient_id])
    end

    # setear historia clinica
    def set_history
      @history = HistoriaClinica.find(params[:id])
    end

    # parametros fuertes
    def history_params
      params.require(:historia_clinica).permit(:id, :paciente_id,
        :antecedentes_personales, :cirugias, :examenes_clinicos, :embarazos,
        :partos, :cesarea, :hijos_vivos, :abortos, :fum, :embarazo_actual,
        :medicamentos, :antecedentes_familiares, :motivo_consulta, :tiempo_gestacion,
        :riesgo_embarazo, :riesgo_embarazo_razon, :patologicos, :hospitalarios,
        :alergicos, :ayudas_previas,
        prenatale_attributes: [:id, :embarazo_riesgo,
          :planeado, :controles, :amenaza_aborto, :print],
        perinatale_attributes: [:id, :nacimiento,
          :cesarias, :vaginal, :print],
        postnatale_attributes: [:id, :complicaciones,
          :lactancia_exclusiva, :print])
    end
  end
end