module Fisio
  class EvolucionesController < FisioController
    before_action :authenticate_usuario!
    before_action :set_patient
    before_action :set_evolucion, except: [:new, :create, :index]

    def index
      @evoluciones = @patient.evolucions.order(created_at: :desc).paginate(page: params[:page], per_page: params[:per_page])
    end

    def new
      @evolucion = @patient.evolucions.new
    end

    def edit
    end

    def create
      @evolucion =  @patient.evolucions.new(evolucion_params)
      if @evolucion.save
        flash[:notice] = 'Registro creado'
        redirect_to fisio_patient_evoluciones_path(@patient)
      else
        render :new
      end
    end

    def update
      if @evolucion.update(evolucion_params)
        flash[:notice] = 'Registro editado'
        redirect_to edit_fisio_patient_evolucione_path(@patient, @evolucion)
      else
        render :edit
      end
    end

    private

    # setear paciente atual
    def set_patient
      @patient = current_usuario.pacientes.find(params[:patient_id])
    end

    def set_evolucion
      @evolucion = @patient.evolucions.find(params[:id])
    end

    def evolucion_params
      params.require(:evolucion).permit(:id, :paciente_id, :descripcion, :creation_date, :is_important)
    end
  end
end

