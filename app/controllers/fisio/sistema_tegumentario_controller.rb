module Fisio
  class SistemaTegumentarioController < FisioController
    before_action :authenticate_usuario!
    before_action :set_patient
    before_action :set_history
    before_action :set_tegu, except: [:new, :create]

    def new
      @tegu = @history.build_sistema_tegumentario
    end

    def edit
    end

    def create
      @tegu = SistemaTegumentario.new(tegu_params)
      if @tegu.save
        flash[:notice] = 'Registro editado'
        redirect_to edit_fisio_patient_clinic_history_sistema_tegumentario_path(@patient, @history, @tegu)
      else
        render :edit
      end
    end

    def update
      if @tegu.update(tegu_params)
        flash[:notice] = 'Registro editado'
        redirect_to edit_fisio_patient_clinic_history_sistema_tegumentario_path(@patient, @history, @tegu)
      else
        render :edit
      end
    end

    private

    # setear paciente atual
    def set_patient
      @patient = current_usuario.pacientes.find(params[:patient_id])
    end

    # setear historia clinica
    def set_history
      @history = HistoriaClinica.find(params[:clinic_history_id])
    end

    def set_tegu
      @tegu = SistemaTegumentario.find(params[:id])
    end

    # parametros fuertes
    def tegu_params
      params.require(:sistema_tegumentario).permit(:id, :historia_clinica_id,
        :color, :estado, :tumefaccion, :escaras, :heridas, :cicatriz)
    end
  end
end
