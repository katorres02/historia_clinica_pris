module Fisio
  class PatientsController < FisioController
    before_action :authenticate_usuario!
    before_action :set_patient, except: [:new, :create]

    # crear nuevo paciente
    def new
      @patient = current_usuario.pacientes.new
    end

    def create
      @patient = Paciente.new(patient_params)
      if @patient.save
        flash[:notice] = "Paciente creado correctamente"
        redirect_to edit_fisio_patient_path(@patient)
      else
        render :new
      end
    end

    def show
    end

    # formulario de edición
    def edit
    end

    # edición de datos
    def update
      if @patient.update(patient_params)
        flash[:notice] = "Paciente editado"
        redirect_to edit_fisio_patient_path(@patient)
      else
        render :edit
      end
    end

    private

    def set_patient
      @patient = current_usuario.pacientes.find(params[:id])
    end

    def patient_params
      params.require(:paciente).permit(:id, :usuario_id, :estado_civil_id,
      :fecha_actual, :direccion, :edad, :genero, :telefono, :movil, :rh, :membership_id,
      :profesion, :nombre_acompanante, :movil_acompanante, :remitido_por, :eps, :email,
      :type_document_id, :telefono, :escolaridad, :documento, :nombre, :fecha_nacimiento)
    end
  end
end