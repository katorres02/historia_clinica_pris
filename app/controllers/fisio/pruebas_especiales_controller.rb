module Fisio
  class PruebasEspecialesController < FisioController
    before_action :authenticate_usuario!
    before_action :set_patient
    before_action :set_history
    
    def edit
      @history.pruebas_especiales_pacientes.build if @history.pruebas_especiales_pacientes.count == 0
      @history.pruebas_flexibilidad_pacientes.build if @history.pruebas_flexibilidad_pacientes.count == 0
    end

    def update
      if @history.update(pruebas_params)
        flash[:notice] = 'Registro editado'
        redirect_to edit_fisio_patient_clinic_history_pruebas_especiale_path(@patient, @history, 1)
      else
        render :edit
      end
    end

    private

    # setear paciente atual
    def set_patient
      @patient = current_usuario.pacientes.find(params[:patient_id])
    end

    # setear historia clinica
    def set_history
      @history = HistoriaClinica.find(params[:clinic_history_id])
    end

    # parámetros fuertes
    def pruebas_params
      params.require(:historia_clinica).permit(:id,
        pruebas_especiales_pacientes_attributes: [:id, :pruebas_especial_id, :zona, :derecho,
          :izquierdo, :observaciones, :_destroy],
        pruebas_flexibilidad_pacientes_attributes: [:id, :pruebas_flexibilidad_id,
         :musculo, :positivo, :negativo, :leve, :moderado, :severo, :_destroy])
    end
  end
end

