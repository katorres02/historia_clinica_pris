# controlador para el sistema musculoesqueletico
class MusculoesqueleticoController < ApplicationController

  before_action :authenticate_usuario!
  before_action :set_patient

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 23-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: POST
  #
  # Metodo: Crea un sistema cardiopulmonar en la base de datos
  def create
    @musculo = SistemaMusculoesqueletico.new(musculo_params)
    if @musculo.save
      redirect_to clinic_history_patients_path(id: @patient.id), notice: 'Sistema musculoesquelético creado'
    else
      redirect_to clinic_history_patients_path, alert: 'Error al crear el sistema'
    end
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 23-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: PATCH
  #
  # Metodo: Edita un sistema cardiopulmonar en la base de datos
  def update
    musculo = HistoriaClinica.find(musculo_params[:historia_clinica_id]).sistema_musculoesqueletico
    if musculo.update_attributes(musculo_params)
      redirect_to clinic_history_patients_path(id: @patient.id), notice: 'Sistema actualizado'
    else
      redirect_to clinic_history_patients_path(id: @patient.id), alert: 'Error al actualizar'
    end
  end

  private

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 23-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Metodo: Parámetros fuertes para crear un sistema cardiopulmonar
  def musculo_params
    params.require(:sistema_musculoesqueletico).permit(:id, :historia_clinica_id, :talla, :peso, :imc, :grasa_total, :grasa_localizada,
    :m_superior_r_derecho, :m_superior_r_izquierdo, :m_superior_a_derecho, :m_superior_a_izquierdo, :m_superiores_diferencia,
    :m_inferior_r_derecho, :m_inferior_r_izquierdo, :m_inferior_a_derecho, :m_inferior_a_izquierdo, :m_inferiores_diferencia,
                                                       perimetro_attributes: [:id, :biceps, :pecho, :abdomen, :cadera, :muslo, :pierna,
                                                                              :sistema_musculoesqueletico_id],
                                                       adipometrium_attributes: [:id, :triceps, :s_escap, :s_iliaco, :abdomen, :muslo,
                                                                                 :pierna],
                                                        cuello_attributes: [:id, :flexion_derecho, :flexion_izquierdo, :ext_derecho,
                                                                            :ext_izquierdo, :incl_lateral_der, :incl_lateral_izq,
                                                                            :rotacion_lat_der, :rotacion_lat_izq],
                                                        hombro_attributes: [:id, :flexion_derecho, :flexion_izquierdo, :ext_derecho,
                                                                            :ext_izquierdo, :abduccion_der, :abduccion_izq, :aduccion_der,
                                                                            :aduccion_izq, :rotacion_interna_der, :rotacion_interna_izq,
                                                                            :rotacion_externa_der, :rotacion_externa_izq],
                                                       musculos_superior_attributes: [:id, :flexion_codo_der, :flexion_codo_izq, :extension_codo_der,
                                                                            :extension_codo_izq, :supinacion_codo_der, :supinacion_codo_izq,
                                                                            :pronacion_codo_der, :pronacion_codo_izq, :extension_muneca_der,
                                                                            :extension_muneca_izq, :desviacion_ulnar_muneca_der, :desviacion_ulnar_muneca_izq,
                                                                            :desviacion_radial_muneca_der, :desviacion_radial_muneca_izq],
                                                        tronco_attributes: [:id, :flexion_der, :flexion_izq, :extension_der, :extension_izq,
                                                                            :flexion_lateral_der, :flexion_lateral_izq, :rotacion_der, :rotacion_izq],
                                                        cadera_attributes: [:id, :flexion_der, :flexion_izq, :extension_der, :extension_izq, :abduccion_der,
                                                                            :abduccion_izq, :aduccion_der, :aduccion_izq, :rotacion_medial_der,
                                                                            :rotacion_medial_izq, :rotacion_lateral_der, :rotacion_lateral_izq],
                                                        musculos_inferior_attributes: [:id, :flexion_rodilla_der, :flexion_rodilla_izq, :extension_der,
                                                                                       :extension_izq, :dorsflexion_tobillo_der, :dorsflexion_tobillo_izq,
                                                                                       :plantiflexion_der, :plantiflexion_izq, :inversion_tobillo_der,
                                                                                        :inversion_tobillo_izq, :eversion_tobillo_der, :eversion_tobillo_izq],
                                                        dolors_attributes: [:id, :localizacion, :tipo, :reposo, :palpacion, :movimiento, :resistencia,
                                                                            :exaservacion, :_destroy],
                                                        edemas_attributes: [:id, :localizacion, :derecho, :izquierdo, :total, :grado, :_destroy],
                                                        daniels_attributes: [:id, :musculo_cero, :musculo_uno, :musculo_dos, :musculo_tres, :musculo_cuatro, :musculo_cinco, :_destroy])
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 23-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Metodo: Crea una variable de paciente, general para los metodos de
  # este controlador, es llamado en el before action
  def set_patient
    if current_usuario.pacientes.exists?(params[:patient_id])
      @patient = current_usuario.pacientes.find(params[:patient_id])
    else
      redirect_to root_path, alert: 'No tiene permitido acceder'
    end
  end
end
