# controlador para las funciones ajax o webservices de la web
class ServicesController < ApplicationController

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 17-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: POST
  #
  # Metodo: Guarda el logo de la empresa del usuario
  def save_company_logo
    user = Usuario.find(params[:user_id])
    user.logo_empresa = params[:image_company]
    if user.save
      render json: { message: 'Imagen almacenada' }, status: :ok
    else
      render json: { message: 'Error al guardar logo' }, status: :not_found
    end
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 06-12-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: GET
  #
  # Metodo: retorna la lista de pruebas para la sección de pruebas especiales
  # En historia clínica
  def specials_tests
    render json: { message: 'Pruebas especiales', tests: PruebasEspecial.all.pluck(:id, :nombre) }, status: :ok
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 06-12-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: GET
  #
  # Metodo: retorna la lista de pruebas de flexibilidad para la sección de
  # pruebas flexibilidad En historia clínica
  def flex_tests
    render json: { message: 'Pruebas de flexibilidad', tests: PruebasFlexibilidad.all.pluck(:id, :nombre) }, status: :ok
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 15-12-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: GET
  #
  # Metodo: retorna la lista de pruebas de sistema neuromuscular
  def neuro_test
    render json: { message: 'Pruebas de flexibilidad', tests: PruebaNeuromuscular.all.pluck(:id, :nombre) }, status: :ok
  end
end
