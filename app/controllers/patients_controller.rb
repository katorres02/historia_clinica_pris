# controlador de pacientes de usuarios
class PatientsController < ApplicationController

  before_action :authenticate_usuario!

  # Autor: Carlos Anres Torres Cruz
  #
  # Fecha creacion: 13-07-2017
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: POST
  #
  # Metodo: importar datos de un paciente para crear otro
  def import_data
    paciente = Paciente.find(params[:import_paciente_id])
    # clonar info personal del paciente
    paciente_clone = paciente.dup
    paciente_clone.created_at = Time.now
    paciente_clone.fecha_actual = Time.now
    paciente_clone.save
    
    # clonar evoluaciones
    evoluciones_original = paciente.evolucions
    evoluciones_original.map do |e|
      e_clone = e.dup
      e_clone.paciente_id = paciente_clone.id
      e_clone.save
    end

    # clonar historia clinica
    historia_clinica = paciente.historia_clinica
    unless historia_clinica.nil?
      historia_clinica_clone = historia_clinica.dup
      historia_clinica_clone.paciente_id = paciente_clone.id
      historia_clinica_clone.save

      # clonar historia diagnostico
      unless historia_clinica.diagnostico.nil?
        diagnostico_clone = historia_clinica.diagnostico.dup
        diagnostico_clone.historia_clinica_id = historia_clinica_clone.id
        diagnostico_clone.save
      end
    
      # clonar sistema cardiopulmonar
      unless historia_clinica.sistema_cardiopulmonar.nil?
        sistema_cardiopulmonar_clone = historia_clinica.sistema_cardiopulmonar.dup
        sistema_cardiopulmonar_clone.historia_clinica_id = historia_clinica_clone.id
        sistema_cardiopulmonar_clone.save
      end

      # clonar sistema_musculoesqueletico
      unless historia_clinica.sistema_musculoesqueletico.nil?
        sis_mus_esq = historia_clinica.sistema_musculoesqueletico # original
        sistema_musculoesqueletico_clone = historia_clinica.sistema_musculoesqueletico.dup
        sistema_musculoesqueletico_clone.historia_clinica_id = historia_clinica_clone.id
        sistema_musculoesqueletico_clone.save

        # clonar PERIMETRO
        unless sis_mus_esq.perimetro.nil?
          perimetro_clone = sis_mus_esq.perimetro.dup
          perimetro_clone.sistema_musculoesqueletico_id = sistema_musculoesqueletico_clone.id
          perimetro_clone.save
        end

        # clonar ADIPOMETRIUM
        unless sis_mus_esq.adipometrium.nil?
          adipometrium_clone = sis_mus_esq.adipometrium.dup
          adipometrium_clone.sistema_musculoesqueletico_id = sistema_musculoesqueletico_clone.id
          adipometrium_clone.save
        end

        # clonar CUELLO
        unless sis_mus_esq.cuello.nil?
          cuello_clone = sis_mus_esq.cuello.dup
          cuello_clone.sistema_musculoesqueletico_id = sistema_musculoesqueletico_clone.id
          cuello_clone.save
        end

        # clonar HOMBRO
        unless sis_mus_esq.hombro.nil?
          hombro_clone = sis_mus_esq.hombro.dup
          hombro_clone.sistema_musculoesqueletico_id = sistema_musculoesqueletico_clone.id
          hombro_clone.save
        end

        # clonar MUSCULOS SUPERIORES
        unless sis_mus_esq.musculos_superior.nil?
          musculos_superior_clone = sis_mus_esq.musculos_superior.dup
          musculos_superior_clone.sistema_musculoesqueletico_id = sistema_musculoesqueletico_clone.id
          musculos_superior_clone.save
        end

        # clonar TRONCO
        unless sis_mus_esq.tronco.nil?
          tronco_clone = sis_mus_esq.tronco.dup
          tronco_clone.sistema_musculoesqueletico_id = sistema_musculoesqueletico_clone.id
          tronco_clone.save
        end

        # clonar CADERA
        unless sis_mus_esq.cadera.nil?
          cadera_clone = sis_mus_esq.cadera.dup
          cadera_clone.sistema_musculoesqueletico_id = sistema_musculoesqueletico_clone.id
          cadera_clone.save
        end

        # clonar MUSCULOS INFERIORES
        unless sis_mus_esq.musculos_inferior.nil?
          musculos_inferior_clone = sis_mus_esq.musculos_inferior.dup
          musculos_inferior_clone.sistema_musculoesqueletico_id = sistema_musculoesqueletico_clone.id
          musculos_inferior_clone.save
        end

        # clonar DOLORES
        dolores = sis_mus_esq.dolors
        dolores.map do |dolor|
          dolor_clone = dolor.dup
          dolor_clone.sistema_musculoesqueletico_id = sistema_musculoesqueletico_clone.id
          dolor_clone.save
        end

        # clonar EDEMAS
        edemas = sis_mus_esq.edemas
        edemas.map do |edema|
          edema_clone = edema.dup
          edema_clone.sistema_musculoesqueletico_id = sistema_musculoesqueletico_clone.id
          edema_clone.save
        end

        # clonar DANIELS
        daniels = sis_mus_esq.daniels
        daniels.map do |daniel|
          daniel_clone = daniel.dup
          daniel_clone.sistema_musculoesqueletico_id = sistema_musculoesqueletico_clone.id
          daniel_clone.save
        end
      end

      # clonar sistema_tegumentario
      unless historia_clinica.sistema_tegumentario.nil?
        sistema_tegumentario_clone = historia_clinica.sistema_tegumentario.dup
        sistema_tegumentario_clone.historia_clinica_id = historia_clinica_clone.id
        sistema_tegumentario_clone.save
      end

      # clonar sistema_neuromusculars
      neruromuscular = historia_clinica.sistema_neuromusculars
      neruromuscular.map do |neuro|
        neuro_clone = neuro.dup
        neuro_clone.historia_clinica_id = historia_clinica_clone.id
        neuro_clone.save
      end

      # clonar pruebas_especiales_pacientes
      pruebas_especiales = historia_clinica.pruebas_especiales_pacientes
      pruebas_especiales.map do |pe|
        pe_clone = pe.dup
        pe_clone.historia_clinica_id = historia_clinica_clone.id
        pe_clone.save
      end

      # clonar pruebas_flexibilidad_pacientes
      pruebas_flexibilidad = historia_clinica.pruebas_flexibilidad_pacientes
      pruebas_flexibilidad.map do |pf|
        pf_clone = pf.dup
        pf_clone.historia_clinica_id = historia_clinica_clone.id
        pf_clone.save
      end
    end

    redirect_to clinic_history_patients_path(id: paciente_clone.id), notice: 'Datos copiados con éxito'
  end

  # Autor: Carlos Anres Torres Cruz
  #
  # Fecha creacion: 13-07-2017
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: GET
  #
  # Metodo: buscar pacientes y paginarlos
  def search_data
    @result = []
    @result = current_usuario.pacientes.search_filter(params[:search_term]).order(nombre: :asc)
    @result = @result.paginate(:page => params[:page], :per_page => 10)
  end

  # Autor: Vanessa Pineda Diaz
  #
  # Fecha creacion: 16-11-2015
  #
  # Autor actualizacion: Carlos Andres Torres Cruz
  #
  # Fecha actualizacion: 17-11-2015
  #
  # Verbo HTTP: GET
  #
  # Metodo: de encarga de enviar la lista de pacientes a mostrar en la vista,
  # solo se mostraran los pacientes del usuario en sesion
  def index

    if current_usuario.user_services.count == 0
      sign_out current_usuario
      flash[:alert] = "Solicite la asignación de un módulo para trabajar en su cuenta."
      redirect_to root_path
    else
      if params[:search_term].present? && !params[:search_term].empty?
        @patients = current_usuario.pacientes.search_filter(params[:search_term]).order(created_at: :desc).paginate(page: params[:page], per_page: 10)
      else
        @patients = current_usuario.pacientes.order(created_at: :desc).paginate(page: params[:page], per_page: 10)
      end
    end    
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 21-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion: 00-00-0000
  #
  # Verbo Http: DELETE
  #
  # Metodo: Elimina un paciente
  def destroy
    if current_usuario.pacientes.find(params[:id])
      patient = Paciente.find(params[:id])
      patient.destroy
      redirect_to root_path, notice: 'Paciente Eliminado'
    else
      redirect_to root_path, alert: 'No tiene permiso para eliminar este paciente'
    end
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 21-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: GET
  #
  # Metodo: Formulario de registro de paciente, en caso de que no exista
  # un paciente se envia una variable de tipo paciente vacia para el
  # formulario de creación. en caso contrario se envian los datos del
  # a editar
  def clinic_history
    if Paciente.exists?(params[:id]) && current_usuario.pacientes.find(params[:id])
      @patient = Paciente.find(params[:id])
      @clinic_history = @patient.historia_clinica.nil? ? HistoriaClinica.new : @patient.historia_clinica
      @cardiopulmonar = @clinic_history.sistema_cardiopulmonar.nil? ? SistemaCardiopulmonar.new : @clinic_history.sistema_cardiopulmonar

      if @clinic_history.pruebas_especiales_pacientes.nil?
        @clinic_history.pruebas_especiales_pacientes.build
        @special_test_builds = @clinic_history.pruebas_especiales_pacientes.count
      else
        @clinic_history.pruebas_especiales_pacientes.build if @clinic_history.pruebas_especiales_pacientes.empty?
        @special_test_builds = @clinic_history.pruebas_especiales_pacientes.count
      end
      # pruebas de flexibilidad
      if @clinic_history.pruebas_flexibilidad_pacientes.nil?
        @clinic_history.pruebas_flexibilidad_pacientes.build
        @flex_test_builds = @clinic_history.pruebas_flexibilidad_pacientes.count
      else
        @clinic_history.pruebas_flexibilidad_pacientes.build if @clinic_history.pruebas_flexibilidad_pacientes.empty?
        @flex_test_builds = @clinic_history.pruebas_flexibilidad_pacientes.count
      end

      # sistemas neuromusculares
      if @clinic_history.sistema_neuromusculars.nil?
        @clinic_history.sistema_neuromusculars.build
        @neuro_builds = @clinic_history.sistema_neuromusculars.count
      else
        @clinic_history.sistema_neuromusculars.build if @clinic_history.sistema_neuromusculars.empty?
        @neuro_builds = @clinic_history.sistema_neuromusculars.count
      end

      # diagnostico
      @clinic_history.build_diagnostico if @clinic_history.diagnostico.nil?

      # sistema tegumentario
      @clinic_history.build_sistema_tegumentario if @clinic_history.sistema_tegumentario.nil?

      if @clinic_history.sistema_musculoesqueletico.nil?
        @musculoesqueletico = SistemaMusculoesqueletico.new
        @musculoesqueletico.build_perimetro
        @musculoesqueletico.build_adipometrium
        @musculoesqueletico.build_cuello
        @musculoesqueletico.build_hombro
        @musculoesqueletico.build_musculos_superior
        @musculoesqueletico.build_tronco
        @musculoesqueletico.build_cadera
        @musculoesqueletico.build_musculos_inferior
        @musculoesqueletico.dolors.build
        @dolor_builds = @musculoesqueletico.dolors.count #cantidad de builds que tiene el formulario
        @musculoesqueletico.edemas.build
        @edema_builds = @musculoesqueletico.edemas.count #cantidad de builds que tiene el formulario
        @musculoesqueletico.daniels.build
      else
        @musculoesqueletico = @clinic_history.sistema_musculoesqueletico
        @musculoesqueletico.build_perimetro if @musculoesqueletico.perimetro.nil?
        @musculoesqueletico.build_adipometrium if @musculoesqueletico.adipometrium.nil?
        @musculoesqueletico.build_cuello if @musculoesqueletico.cuello.nil?
        @musculoesqueletico.build_hombro if @musculoesqueletico.hombro.nil?
        @musculoesqueletico.build_musculos_superior if @musculoesqueletico.musculos_superior.nil?
        @musculoesqueletico.build_tronco if @musculoesqueletico.tronco.nil?
        @musculoesqueletico.build_cadera if @musculoesqueletico.cadera.nil?
        @musculoesqueletico.build_musculos_inferior if @musculoesqueletico.musculos_inferior.nil?
        @musculoesqueletico.dolors.build if @musculoesqueletico.dolors.empty?
        @dolor_builds = @musculoesqueletico.dolors.count
        @musculoesqueletico.edemas.build if @musculoesqueletico.edemas.empty?
        @edema_builds = @musculoesqueletico.edemas.count
        @musculoesqueletico.daniels.build if @musculoesqueletico.daniels.empty?
      end
    else
      if params[:id].nil?
        @patient = Paciente.new
        @clinic_history = HistoriaClinica.new
        @cardiopulmonar = SistemaCardiopulmonar.new
        @musculoesqueletico = SistemaMusculoesqueletico.new
        @musculoesqueletico.build_perimetro
        @musculoesqueletico.build_adipometrium
        @musculoesqueletico.build_cuello
        @musculoesqueletico.build_hombro
        @musculoesqueletico.build_musculos_superior
        @musculoesqueletico.build_tronco
        @musculoesqueletico.build_cadera
        @musculoesqueletico.build_musculos_inferior
        @musculoesqueletico.dolors.build
        @dolor_builds = @musculoesqueletico.dolors.count
        @musculoesqueletico.edemas.build
        @edema_builds = @musculoesqueletico.edemas.count
        @musculoesqueletico.daniels.build
      else
        redirect_to root_path, alert: 'El paciente no esta asociado a esta cuenta'
      end
    end
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: POST
  #
  # Metodo: Crear historia clinica asociada a un paciente
  def create_clinic_history
    clinic_history = HistoriaClinica.new(clinic_history_params)
    if clinic_history.save
      redirect_to clinic_history_patients_path(id: clinic_history_params[:paciente_id]), notice: 'Historia Clínica creada'
    else
      redirect_to clinic_history_patients_path(id: clinic_history_params[:paciente_id]), alert: 'Error al crear el Historia'
    end
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: PATCH
  #
  # Metodo: Actualiza una historia clínica
  def update_clinic_history
    clinic_history = Paciente.find(clinic_history_params[:paciente_id]).historia_clinica
    if clinic_history.update_attributes(clinic_history_params)
      redirect_to clinic_history_patients_path(id: clinic_history_params[:paciente_id]), notice: 'Historia Clínica actualizada'
    else
      redirect_to clinic_history_patients_path(id: clinic_history_params[:paciente_id]), alert: 'Error al actualizar'
    end
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 21-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: POST
  #
  # Metodo: crea un paciente nuevo
  def create
    patient = Paciente.new(patient_params)
    if patient.save
      redirect_to clinic_history_patients_path(id: patient.id), notice: 'Paciente creado'
    else
      redirect_to clinic_history_patients_path, alert: 'Error al crear el paciente'
    end
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: PATCH
  #
  # Metodo: EDITA un paciente nuevo
  def update
    patient = Paciente.find(patient_params[:id])
    if patient.update_attributes(patient_params)
      redirect_to clinic_history_patients_path(id: patient.id), notice: 'Paciente actualizado'
    else
      redirect_to clinic_history_patients_path(id: patient.id), alert: 'Error al actualizar'
    end
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 10-07-2016
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: PATCH
  #
  # Metodo: impresión de historia clínica
  def print
    @patient = Paciente.find(params[:id])
    respond_to do |format|
      format.html
      format.pdf do
        render template: "patients/print", pdf: "#{@patient.nombre}.Historia Clinica",
               encoding: 'utf-8', :margin => {:bottom => 22}, footer:  { html: {   template:'shared/pdf_footer' } }
      end
    end
  end

  private

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 21-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Metodo: parametros fuertes para crear un paciente
  def patient_params
    params.require(:paciente).permit(:id, :usuario_id, :estado_civil_id, :documento, :nombre, :fecha_nacimiento,
                                     :fecha_actual, :direccion, :edad, :genero, :telefono, :movil, :rh, :membership_id,
                                     :profesion, :nombre_acompanante, :movil_acompanante, :remitido_por, :eps, :email,
                                     :type_document_id, :telefono, :escolaridad)
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Metodo: parametros fuertes para crear una hisotira clinica
  def clinic_history_params
    params.require(:historia_clinica).permit(:id, :paciente_id, :antecedentes_personales, :cirugias,
                                             :examenes_clinicos, :embarazos, :partos, :cesarea,
                                             :hijos_vivos, :abortos, :fum, :embarazo_actual, :medicamentos,
                                             :antecedentes_familiares, :motivo_consulta,
                                             pruebas_especiales_pacientes_attributes: [:id, :pruebas_especial_id,
                                                  :zona, :derecho, :izquierdo, :observaciones, :_destroy],
                                             pruebas_flexibilidad_pacientes_attributes: [:id, :pruebas_flexibilidad_id,
                                                                                       :musculo, :positivo, :negativo, :leve,
                                                                                         :moderado, :severo, :_destroy],
                                             sistema_neuromusculars_attributes: [:id, :prueba_neuromuscular_id, :localizacion,
                                                                                  :resultado, :observacion, :_destroy],

                                             sistema_tegumentario_attributes: [:id, :color, :estado, :tumefaccion, :escaras,
                                                                               :heridas, :cicatriz],
                                             diagnostico_attributes: [:id, :diagnostico, :recomendacion, :restriccion,
                                                                               :_destroy])
  end
end

