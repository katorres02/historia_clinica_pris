# controlador para las evoluciones del paciente
class EvolutionsController < ApplicationController
  before_action :authenticate_usuario!
  before_action :set_patient

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: GET
  #
  # Metodo: Muestra las evoluciones de un paciente
  def index
    @evolutions = current_usuario.pacientes.find(params[:patient_id]).evolucions
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: GET
  #
  # Metodo: Formulario para crear una evolución
  def new
    @evolution = Evolucion.new
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: POST
  #
  # Metodo: Crear una evolución en la base de datos
  def create
    evolution = Evolucion.new(evolution_params)
    if evolution.save
      redirect_to patient_evolutions_path, notice: 'Evolución creada con éxito'
    else
      redirect_to new_patient_evolution(patient_id: evolution_params[:paciente_id]),
                  alert: 'Error al crer evolución'
    end
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: GET
  #
  # Metodo: Formulario para editar una evolucion
  def edit
    @evolution = Evolucion.find(params[:id])
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo Http: DELETE
  #
  # Metodo: Elimina una evolución
  def destroy
    if current_usuario.pacientes.find(params[:patient_id]).evolucions.exists?(params[:id])
      evolution = Evolucion.find(params[:id])
      evolution.destroy
      redirect_to patient_evolutions_path, notice: 'Evolución Eliminada'
    else
      redirect_to patient_evolutions_path, alert: 'No tiene permiso para eliminar esta evolución'
    end
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo Http: DELETE
  #
  # Metodo: Edita una evolución
  def update
    evolution = Evolucion.find(params[:id])
    if evolution.update_attributes(evolution_params)
      redirect_to patient_evolutions_path, notice: 'Evolución actualizada'
    else
      redirect_to patient_evolutions_path, alert: 'Error al actualizar'
    end
  end

  private

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Metodo: Parámetros fuertes de la tabla evolución
  def evolution_params
    params.require(:evolucion).permit(:paciente_id, :descripcion, :creation_date)
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Metodo: Crea una variable de paciente, general para los metodos de
  # este controlador, es llamado en el before action
  def set_patient
    if current_usuario.pacientes.exists?(params[:patient_id])
      @patient = current_usuario.pacientes.find(params[:patient_id])
    else
      redirect_to root_path, alert: 'No tiene permitido acceder'
    end
  end
end
