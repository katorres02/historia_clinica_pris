# controlador para el sistema cardiopulmonar de una historia clínica
class CardiopulmonarController < ApplicationController

  before_action :authenticate_usuario!
  before_action :set_patient

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: POST
  #
  # Metodo: Crea un sistema cardiopulmonar en la base de datos
  def create
    cardio = SistemaCardiopulmonar.new(cardio_params)
    if cardio.save
      redirect_to clinic_history_patients_path(id: @patient.id), notice: 'Sistema cardiopulmonar creado'
    else
      redirect_to clinic_history_patients_path, alert: 'Error al crear el sistema'
    end
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Verbo HTTP: PATCH
  #
  # Metodo: Edita un sistema cardiopulmonar en la base de datos
  def update
    cardio = HistoriaClinica.find(cardio_params[:historia_clinica_id]).sistema_cardiopulmonar
    if cardio.update_attributes(cardio_params)
      redirect_to clinic_history_patients_path(id: @patient.id), notice: 'Sistema actualizado'
    else
      redirect_to clinic_history_patients_path(id: @patient.id), alert: 'Error al actualizar'
    end
  end

  private

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Metodo: Parámetros fuertes para crear un sistema cardiopulmonar
  def cardio_params
    params.require(:sistema_cardiopulmonar).permit(:id, :historia_clinica_id, :pa, :fc, :fr,
                                                   :sao2, :ausc_pulmonar, :ausc_cardiaca, :patient_id)
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 22-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Metodo: Crea una variable de paciente, general para los metodos de
  # este controlador, es llamado en el before action
  def set_patient
    if current_usuario.pacientes.exists?(params[:patient_id])
      @patient = current_usuario.pacientes.find(params[:patient_id])
    else
      redirect_to root_path, alert: 'No tiene permitido acceder'
    end
  end
end
