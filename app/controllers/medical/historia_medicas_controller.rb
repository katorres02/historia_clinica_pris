module Medical
  class HistoriaMedicasController < MedicalController
    before_action :set_patient
    before_action :set_history, only: [:edit, :update]

    def index
      @histories = @patient.historia_medicas.order('created_at desc')
      .paginate(page: params[:page], per_page: 15)
    end

    def new
      @history = @patient.historia_medicas.new
      @history.impresion_diagnosticas.build
    end

    def edit
      @history.impresion_diagnosticas.build if @history.impresion_diagnosticas.count == 0
    end

    def create
      @history = HistoriaMedica.new(history_params)
      if @history.save
        flash[:notice] = "Historia clínica creada"
        redirect_to edit_medical_patient_historia_medica_path(@patient, @history)
      else
        render :new
      end
    end

    # editar historia solo hasta 15 minutos despues
    def update
      if  (Time.now - 15.minutes) > @history.created_at
        @history.errors.add("Tiempo", "Solo puede realizar cambios hasta 15 minutos después de crear la historia clíinica.")
        render :edit
      else
        if @history.update(history_params)
          flash[:notice] = "Información actualizada"
          redirect_to edit_medical_patient_historia_medica_path(@patient, @history)
        else
          render :edit
        end
      end
    end

    private

    # setear paciente atual
    def set_patient
      @patient = current_usuario.pacientes.find(params[:patient_id])
    end

    # setear historia clinica
    def set_history
      @history = HistoriaMedica.find(params[:id])
    end

    # parametros fuertes
    def history_params
      params.require(:historia_medica).permit(:id, :paciente_id, :motivo_consulta, :piel_faneras,
        :historia_actual, :antecedente_medico, :antecedente_quirurgico, :antecedente_ginecoobstetrico,
        :antecedente_alergia, :revision_sistemas, :imagenes, :laboratorios, :exploracion_fisica,
        :inspeccion_general, :cabeza, :cuello, :torax, :abdomen, :genitales, :extremidades, :neurologico,
        :mental, :analisis, :plan, :intervencion_terapeutica, :formulacion, :control,
        :ta, :fc, :fr, :sato2, :peso, :talla, :glucometria, :imc, :temperatura, :unidad_talla,
        impresion_diagnosticas_attributes: [:id, :historia_medica_id, :descripcion, :_destroy])
    end
  end
end


