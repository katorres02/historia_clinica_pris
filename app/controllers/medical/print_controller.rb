module Medical
  class PrintController < MedicalController
    before_action :set_patient
    before_action :set_history
    
    def single
      respond_to do |format|
        format.html
        format.pdf do
          render template: "medical/historia_medicas/print", pdf: "#{@patient.nombre}-Historia Clinica-#{@history.created_at.strftime('%d-%m-%Y')}",
          encoding: 'utf-8', :margin => {:bottom => 22}, footer:  { html: {   template:'shared/pdf_footer' } }
        end
      end
    end

    # solo campo formulación
    def formulacion
      respond_to do |format|
        format.html
        format.pdf do
          render template: "medical/historia_medicas/print_formulacion", pdf: "#{@patient.nombre}-Formulación-#{@history.created_at.strftime('%d-%m-%Y')}",
          encoding: 'utf-8', :margin => {:bottom => 22}, footer:  { html: {   template:'shared/pdf_footer' } }
        end
      end
    end

    private

    # setear paciente atual
    def set_patient
      @patient = current_usuario.pacientes.find(params[:patient_id])
    end

    # setear historia clinica
    def set_history
      @history = HistoriaMedica.where(id: params[:historia_medica_id]).includes(:impresion_diagnosticas).first
    end
  end
end

