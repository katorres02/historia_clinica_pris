# Controlador para funcionalidades propias del home
class HomesController < ApplicationController
  # llamado de modulo general DDD
  include GeneralModule

  # Autor: Jackson Florez Jimenez
  #
  # Fecha creacion: 09-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion: 00-00-0000
  #
  # Verbo Http: GET
  #
  # Metodo: Index
  def index
    # llamado de consola en las vistas
    console
    # Metodo: de prueba para revisar el funcionamiendo
    # de la arquitectura DDD(Domain Driven Design)
    example_method_ddd('3', '4') == 7 ? @prueba_ddd = true : @prueba_ddd = false
  end

  private
  # Autor: Jackson Florez Jimenez
  #
  # Fecha creacion: 09-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion: 00-00-0000
  #
  # Parametros fuertes
  # def contact_params
  #  params.require(:contact).permit(:id, :name)
  # end
end
