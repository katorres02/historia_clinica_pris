# modulo fisio, clase generadora de controladores
class MedicalController < ApplicationController
  before_action :authenticate_usuario!
  before_action :check_module

  layout "medical/layouts/application"

  protected

  # validar acceso solo usuarios con este modulo
  def check_module
    service = current_usuario.user_services.find_by(service_id: 1)
    if service.nil?
      redirect_to root_path, alert: 'No tiene acceso a ese módulo.'
    end
  end
end