# modulo fisio, clase generadora de controladores
class FisioController < ApplicationController
  before_action :check_module

  layout "fisio/layouts/application"

  protected

  # validar acceso solo usuarios con este modulo
  def check_module
    if usuario_signed_in?
      service = current_usuario.user_services.find_by(service_id: 2)
      if service.nil?
        redirect_to root_path, alert: 'No tiene acceso a ese módulo.'
      end
    else
      redirect_to root_path, alert: 'Debe iniciar sesisión'
    end
  end
end