# controlador alterno para registro de usuarios con devise
class DeviseLoginsController < Devise::RegistrationsController

  skip_before_action :verify_authenticity_token
  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 12-10-2015
  #
  # Autor actualizacion:
  #
  # Verbo Http: Get
  #
  # Metodo: este metodo es llamado por devise al querer recuperar la
  # contrasena de la cuenta
  # internamente devise hace el envio de correo, pero podemos programar
  # el comportamiento de la app despues de eso, en este caso solo hace que
  # redireccione a la vista home
  def new
    redirect_to root_path
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 09-10-2015
  #
  # Autor actualizacion:
  #
  # Verbo Http: Post
  #
  # Metodo: valida si el correo y contrasena son correctos y genera una
  # sesion en caso de ser atenticado
  def create
    if Usuario.exists?(email: sign_in_params[:email])
      user = Usuario.find_by(email: sign_in_params[:email])

      if user.activo
        if user.valid_password?(sign_in_params[:password])
          set_flash_message :notice, :signed_in if is_flashing_format?
          sign_in_and_redirect user, event: :authentication
        else
          redirect_to root_path, alert: 'Correo o contraseña invalidos'
        end
      else
        redirect_to root_path, alert: 'Esta cuenta se encuentra suspendida, por favor contacte con soporte al cliente para solucionar el problema'
      end
    else
      redirect_to root_path, alert: 'El usuario no está registrado, por favor contacte con servicio al cliente'
    end
  end

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 06-01-2016
  #
  # Autor actualizacion:
  #
  # Verbo Http: Post
  #
  # Metodo: cierra la sesion del usuario
  def destroy
    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
    set_flash_message :notice, :signed_out if is_flashing_format?
    yield resource if block_given?
    respond_with_navigational(resource){ redirect_to after_sign_out_path_for(resource_name) }
  end

  private

  # Autor: Carlos Andres Torres Cruz
  #
  # Fecha creacion: 09-10-2015
  #
  # Autor actualizacion:
  #
  # Metodo: permite paramatros nuevos a los que tra el devise por defecto,
  # en este caso el campo name, este metodo sobreescribe el usado por
  # devise al crear usuario
  def sign_in_params
    params.require(:usuario).permit(:email, :password)
  end
end
