ActiveAdmin.register Usuario do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


  form title: 'A custom title' do |f|
    inputs 'Details' do
      input :nombre
      input :apellido
      input :documento
      input :direccion
      input :telefono
      input :movil

      input :email
      input :password
      
      input :activo

      input :nombre_empresa
      input :logo_empresa
      input :tarjeta_profesional
      input :firma_digital
      #input :can_edit

      f.inputs do
        f.has_many :user_services, heading: 'Modulo', allow_destroy: true do |a|
          a.input :service, label: 'Servicio'
        end
      end
    end



    para "Presione cancelar para volver sin guardar"
    actions
  end

  controller do
    def update
      if params[:usuario][:password].blank? && params[:usuario][:password_confirmation].blank?
        params[:usuario].delete("password")
        params[:usuario].delete("password_confirmation")
      end
      super
    end
  end


  permit_params :email, :password, :activo, :nombre, :apellido, :nombre_empresa, :logo_empresa, :documento, :tarjeta_profesional, :firma_digital,
    :telefono, :movil, :direccion, :can_edit,
    user_services_attributes: [:id, :service_id, :usuario_id, :_destroy]


end
