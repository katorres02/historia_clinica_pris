class CreateClasificacionInternacionals < ActiveRecord::Migration
  def change
    create_table :clasificacion_internacionals do |t|
      t.references :sistema_neuromuscular, index: true, foreign_key: true
      t.string :deficiencia_principal
      t.string :limitacion_actividades
      t.string :factores_ambientales
      t.string :observacion

      t.timestamps null: false
    end
  end
end
