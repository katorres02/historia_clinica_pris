class CreateTroncos < ActiveRecord::Migration
  def change
    create_table :troncos do |t|
      t.references :sistema_musculoesqueletico, index: true, foreign_key: true
      t.string :flexion_der
      t.string :flexion_izq
      t.string :extension_der
      t.string :extension_izq
      t.string :flexion_lateral_der
      t.string :flexion_lateral_izq
      t.string :rotacion_der
      t.string :rotacion_izq

      t.timestamps null: false
    end
  end
end
