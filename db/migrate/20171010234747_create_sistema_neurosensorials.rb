class CreateSistemaNeurosensorials < ActiveRecord::Migration
  def change
    create_table :sistema_neurosensorials do |t|
      t.references :sistema_neuromuscular, index: true, foreign_key: true
      t.boolean :vestirse
      t.boolean :disfruta_banio
      t.boolean :cepilla
      t.boolean :molesta_ensucia
      t.boolean :molesta_sonido
      t.boolean :busca_sonido
      t.boolean :responde_llamado
      t.boolean :disfruta_estimulo
      t.boolean :persecucion_ocular
      t.boolean :superficie_inestable
      t.boolean :juegos_mecanicos
      t.boolean :juegos_aparatos

      t.timestamps null: false
    end
  end
end
