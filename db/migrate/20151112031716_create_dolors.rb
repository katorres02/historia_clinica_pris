class CreateDolors < ActiveRecord::Migration
  def change
    create_table :dolors do |t|
      t.references :sistema_musculoesqueletico, index: true, foreign_key: true
      t.string :localizacion
      t.string :tipo
      t.string :reposo
      t.string :palpacion
      t.string :movimiento
      t.string :resistencia
      t.string :exaservacion

      t.timestamps null: false
    end
  end
end
