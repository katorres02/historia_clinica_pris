class CreatePruebasPosturas < ActiveRecord::Migration
  def change
    create_table :pruebas_posturas do |t|
      t.references :historia_clinica, index: true, foreign_key: true
      t.string :espalda
      t.string :cinetica_inf_string
      t.string :cinetica_sup

      t.timestamps null: false
    end
  end
end
