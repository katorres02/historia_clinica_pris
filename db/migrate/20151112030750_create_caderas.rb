class CreateCaderas < ActiveRecord::Migration
  def change
    create_table :caderas do |t|
      t.references :sistema_musculoesqueletico, index: true, foreign_key: true
      t.string :flexion_der
      t.string :flexion_izq
      t.string :extension_der
      t.string :extension_izq
      t.string :abduccion_der
      t.string :abduccion_izq
      t.string :aduccion_der
      t.string :aduccion_izq
      t.string :rotacion_medial_der
      t.string :rotacion_medial_izq
      t.string :rotacion_lateral_der
      t.string :rotacion_lateral_izq

      t.timestamps null: false
    end
  end
end
