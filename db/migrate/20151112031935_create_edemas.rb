class CreateEdemas < ActiveRecord::Migration
  def change
    create_table :edemas do |t|
      t.references :sistema_musculoesqueletico, index: true, foreign_key: true
      t.string :localizacion
      t.float :derecho
      t.float :izquierdo
      t.float :total
      t.string :grado

      t.timestamps null: false
    end
  end
end
