class AddUnidadTallaToSistemaMusculoesqueleticos < ActiveRecord::Migration
  def change
    add_column :sistema_musculoesqueleticos, :unidad_talla, :integer, default: 1
    add_column :historia_medicas, :unidad_talla, :integer, default: 1
  end
end
