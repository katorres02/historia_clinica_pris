class AddFieldsToHistoriaMedica < ActiveRecord::Migration
  def change
    add_column :historia_medicas, :ta, :string
    add_column :historia_medicas, :fc, :string
    add_column :historia_medicas, :fr, :string
    add_column :historia_medicas, :sato2, :string
    add_column :historia_medicas, :peso, :string
    add_column :historia_medicas, :talla, :string
    add_column :historia_medicas, :glucometria, :string
    add_column :historia_medicas, :imc, :string
  end
end
