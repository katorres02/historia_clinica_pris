class CreatePosturas < ActiveRecord::Migration
  def change
    create_table :posturas do |t|
      t.references :sistema_musculoesqueletico, index: true, foreign_key: true
      t.string :cabeza_cuello
      t.string :hombros
      t.string :tronco
      t.string :miembros_superiores
      t.string :miembros_inferiores
      t.string :columna
      t.string :pelvis
      t.string :observacion

      t.timestamps null: false
    end
  end
end
