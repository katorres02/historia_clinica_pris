class CreateDesarrolloMotors < ActiveRecord::Migration
  def change
    create_table :desarrollo_motors do |t|
      t.references :historia_clinica, index: true, foreign_key: true
      t.string :centro_cefalico
      t.string :sedente
      t.string :rolado
      t.string :gateo
      t.string :bipedo
      t.string :marcha
      t.string :adicion#, prono sobre antebrazos, sedente largo, sedente corto, rodillas, semirodillas
      t.string :antecedentes_psicosociales
      t.string :nucleo_familiar
      t.string :escolaridad
      t.string :habitos

      t.timestamps null: false
    end
  end
end
