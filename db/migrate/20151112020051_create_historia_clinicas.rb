class CreateHistoriaClinicas < ActiveRecord::Migration
  def change
    create_table :historia_clinicas do |t|
      t.references :paciente, index: true, foreign_key: true
      t.text :antecedentes_personales
      t.text :cirugias
      t.text :examenes_clinicos
      t.integer :embarazos
      t.integer :partos
      t.integer :cesarea
      t.integer :hijos_vivos
      t.integer :abortos
      t.date :fum
      t.string :embarazo_actual
      t.text :medicamentos
      t.text :antecedentes_familiares
      t.text :motivo_consulta

      t.string :tiempo_gestacion
      t.boolean :riesgo_embarazo
      t.text :riesgo_embarazo_razon
      t.text :patologicos
      t.text :hospitalarios
      t.text :alergicos
      t.text :ayudas_previas

      t.timestamps null: false
    end
  end
end
