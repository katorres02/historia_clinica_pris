class CreateSistemaCardiopulmonars < ActiveRecord::Migration
  def change
    create_table :sistema_cardiopulmonars do |t|
      t.references :historia_clinica, index: true, foreign_key: true
      t.string :pa
      t.string :fc
      t.string :fr
      t.string :sao2
      t.string :ausc_pulmonar
      t.string :ausc_cardiaca

      t.timestamps null: false
    end
  end
end
