class CreateMemoria < ActiveRecord::Migration
  def change
    create_table :memoria do |t|
      t.references :sistema_neuromuscular, index: true, foreign_key: true
      t.string :anterograda
      t.string :retrogada
      t.string :queja_memoria


      t.timestamps null: false
    end
  end
end
