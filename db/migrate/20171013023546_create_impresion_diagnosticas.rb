class CreateImpresionDiagnosticas < ActiveRecord::Migration
  def change
    create_table :impresion_diagnosticas do |t|
      t.references :historia_medica, index: true, foreign_key: true
      t.text :descripcion

      t.timestamps null: false
    end
  end
end
