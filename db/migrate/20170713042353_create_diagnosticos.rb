class CreateDiagnosticos < ActiveRecord::Migration
  def change
    create_table :diagnosticos do |t|
      t.references :historia_clinica, index: true, foreign_key: true
      t.string :diagnostico
      t.string :recomendacion
      t.string :restriccion
      t.string :sugerencia_terapeutica
      t.string :objetivo_general
      t.string :objetivo_especifico

      t.timestamps null: false
    end
  end
end
