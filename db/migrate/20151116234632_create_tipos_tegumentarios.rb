class CreateTiposTegumentarios < ActiveRecord::Migration
  def change
    create_table :tipos_tegumentarios do |t|
      t.string :nombre

      t.timestamps null: false
    end
  end
end
