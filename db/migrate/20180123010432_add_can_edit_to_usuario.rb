class AddCanEditToUsuario < ActiveRecord::Migration
  def change
    add_column :usuarios, :can_edit, :boolean
  end
end
