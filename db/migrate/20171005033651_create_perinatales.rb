class CreatePerinatales < ActiveRecord::Migration
  def change
    create_table :perinatales do |t|
      t.references :historia_clinica, index: true, foreign_key: true
      t.string :nacimiento
      t.boolean :cesarias
      t.boolean :vaginal

      t.timestamps null: false
    end
  end
end
