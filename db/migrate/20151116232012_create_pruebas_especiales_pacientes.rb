class CreatePruebasEspecialesPacientes < ActiveRecord::Migration
  def change
    create_table :pruebas_especiales_pacientes do |t|
      t.references :historia_clinica, index: true, foreign_key: true
      t.references :pruebas_especial, index: true, foreign_key: true
      t.string :zona
      t.string :derecho
      t.string :izquierdo
      t.text :observaciones

      t.timestamps null: false
    end
  end
end
