class CreateTransicions < ActiveRecord::Migration
  def change
    create_table :transicions do |t|
      t.references :sistema_neuromuscular, index: true, foreign_key: true
      t.string :decubito_prono
      t.string :supino
      t.string :decubito_lateral
      t.string :sedente_bipedo

      t.timestamps null: false
    end
  end
end
