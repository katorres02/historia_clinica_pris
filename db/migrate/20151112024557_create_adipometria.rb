class CreateAdipometria < ActiveRecord::Migration
  def change
    create_table :adipometria do |t|
      t.references :sistema_musculoesqueletico, index: true, foreign_key: true
      t.float :triceps
      t.float :s_escap
      t.float :s_iliaco
      t.float :abdomen
      t.float :muslo
      t.float :pierna

      t.timestamps null: false
    end
  end
end
