class CreateMusculosSuperiors < ActiveRecord::Migration
  def change
    create_table :musculos_superiors do |t|
      t.references :sistema_musculoesqueletico, index: true, foreign_key: true
      t.string :flexion_codo_der
      t.string :flexion_codo_izq
      t.string :extension_codo_der
      t.string :extension_codo_izq
      t.string :supinacion_codo_der
      t.string :supinacion_codo_izq
      t.string :pronacion_codo_der
      t.string :pronacion_codo_izq
      t.string :extension_muneca_der
      t.string :extension_muneca_izq
      t.string :desviacion_ulnar_muneca_der
      t.string :desviacion_ulnar_muneca_izq
      t.string :desviacion_radial_muneca_der
      t.string :desviacion_radial_muneca_izq

      t.timestamps null: false
    end
  end
end
