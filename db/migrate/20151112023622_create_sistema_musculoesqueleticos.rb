class CreateSistemaMusculoesqueleticos < ActiveRecord::Migration
  def change
    create_table :sistema_musculoesqueleticos do |t|
      t.references :historia_clinica, index: true, foreign_key: true
      t.float :talla
      t.float :peso
      t.float :imc
      t.float :grasa_total
      t.float :grasa_localizada
      t.float :m_superior_r_derecho
      t.float :m_superior_r_izquierdo
      t.float :m_superior_a_derecho
      t.float :m_superior_a_izquierdo
      t.float :m_superiores_diferencia
      t.float :m_inferior_r_derecho
      t.float :m_inferior_r_izquierdo
      t.float :m_inferior_a_derecho
      t.float :m_inferior_a_izquierdo
      t.float :m_inferiores_diferencia

      t.timestamps null: false
    end
  end
end
