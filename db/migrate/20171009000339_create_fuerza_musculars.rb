class CreateFuerzaMusculars < ActiveRecord::Migration
  def change
    create_table :fuerza_musculars do |t|
      t.references :sistema_musculoesqueletico, index: true, foreign_key: true
      t.string :cuello_i
      t.string :cuello_d
      t.string :hombro_i
      t.string :hombro_d
      t.string :codo_i
      t.string :codo_d
      t.string :muneca_i
      t.string :muneca_d
      t.string :tronco_i
      t.string :tronco_d
      t.string :miembro_superior_i
      t.string :miembro_superior_d
      t.string :miembro_inferior_i
      t.string :miembro_inferior_d
      t.boolean :ayudas
      t.string :ayudas_text


      t.timestamps null: false
    end
  end
end
