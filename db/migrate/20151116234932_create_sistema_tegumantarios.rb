class CreateSistemaTegumantarios < ActiveRecord::Migration
  def change
    create_table :sistema_tegumentarios do |t|
      t.references :historia_clinica, index: true, foreign_key: true
      t.references :tipos_tegumentario, index: true, foreign_key: true
      t.string :color
      t.string :estado
      t.string :edema
      t.boolean :tumefaccion
      t.boolean :escaras
      t.boolean :heridas
      t.string :cicatriz

      t.timestamps null: false
    end
  end
end
