class CreatePruebasEspecials < ActiveRecord::Migration
  def change
    create_table :pruebas_especials do |t|
      t.string :nombre

      t.timestamps null: false
    end
  end
end
