class CreateMusculosInferiors < ActiveRecord::Migration
  def change
    create_table :musculos_inferiors do |t|
      t.references :sistema_musculoesqueletico, index: true, foreign_key: true
      t.string :flexion_rodilla_der
      t.string :flexion_rodilla_izq
      t.string :extension_der
      t.string :extension_izq
      t.string :dorsflexion_tobillo_der
      t.string :dorsflexion_tobillo_izq
      t.string :plantiflexion_der
      t.string :plantiflexion_izq
      t.string :inversion_tobillo_der
      t.string :inversion_tobillo_izq
      t.string :eversion_tobillo_der
      t.string :eversion_tobillo_izq

      t.timestamps null: false
    end
  end
end
