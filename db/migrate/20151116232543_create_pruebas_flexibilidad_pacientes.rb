class CreatePruebasFlexibilidadPacientes < ActiveRecord::Migration
  def change
    create_table :pruebas_flexibilidad_pacientes do |t|
      t.references :historia_clinica, index: true, foreign_key: true
      t.references :pruebas_flexibilidad, index: true, foreign_key: true
      t.string :musculo
      t.string :positivo
      t.string :negativo
      t.string :leve
      t.string :moderado
      t.string :severo

      t.timestamps null: false
    end
  end
end
