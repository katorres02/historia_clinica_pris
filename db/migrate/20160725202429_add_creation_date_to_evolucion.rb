class AddCreationDateToEvolucion < ActiveRecord::Migration
  def change
    add_column :evolucions, :creation_date, :date
  end
end
