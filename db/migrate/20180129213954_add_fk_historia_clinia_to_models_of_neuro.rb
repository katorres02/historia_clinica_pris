class AddFkHistoriaCliniaToModelsOfNeuro < ActiveRecord::Migration
  def change
  	 add_reference :transicions, :historia_clinica, index: true
  	 add_reference :sistema_neurosensorials, :historia_clinica, index: true
  	 add_reference :memoria, :historia_clinica, index: true
  	 add_reference :psicomotricidads, :historia_clinica, index: true
  	 add_reference :coordinacion_motoras, :historia_clinica, index: true
  	 add_reference :actividads, :historia_clinica, index: true
  	 add_reference :clasificacion_internacionals, :historia_clinica, index: true
  end
end
