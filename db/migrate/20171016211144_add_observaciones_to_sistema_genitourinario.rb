class AddObservacionesToSistemaGenitourinario < ActiveRecord::Migration
  def change
    add_column :sistema_genitourinarios, :observaciones, :text
  end
end
