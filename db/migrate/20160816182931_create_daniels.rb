class CreateDaniels < ActiveRecord::Migration
  def change
    create_table :daniels do |t|
      t.string :musculo_cero
      t.string :musculo_uno
      t.string :musculo_dos
      t.string :musculo_tres
      t.string :musculo_cuatro
      t.string :musculo_cinco
      t.references :sistema_musculoesqueletico

      t.timestamps null: false
    end
  end
end
