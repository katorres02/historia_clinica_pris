class CreatePruebasFlexibilidads < ActiveRecord::Migration
  def change
    create_table :pruebas_flexibilidads do |t|
      t.string :nombre

      t.timestamps null: false
    end
  end
end
