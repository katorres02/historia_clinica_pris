class CreatePsicomotricidads < ActiveRecord::Migration
  def change
    create_table :psicomotricidads do |t|
      t.references :sistema_neuromuscular, index: true, foreign_key: true
      t.boolean :lanzamiento
      t.boolean :pateo
      t.boolean :recepcion
      t.boolean :velocidad
      t.boolean :tamborilear
      t.boolean :escritura
      t.boolean :salto_bipodal
      t.boolean :salto_unipodal
      t.boolean :ascenso_descenso
      t.boolean :pedaleo
      t.boolean :trepado
      t.boolean :carrera
      t.string :dominancia
      t.boolean :lateralidad
      t.string :nocion_cuerpo

      t.timestamps null: false
    end
  end
end
