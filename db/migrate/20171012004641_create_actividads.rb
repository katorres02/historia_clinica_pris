class CreateActividads < ActiveRecord::Migration
  def change
    create_table :actividads do |t|
      t.references :sistema_neuromuscular, index: true, foreign_key: true
      t.boolean :vestirse
      t.boolean :tomar_banio
      t.boolean :levantar_sentar
      t.boolean :subir_bajar_escalera
      t.boolean :subir_bajar_rampa
      t.boolean :caminar_barrio
      t.boolean :preparar_comida
      t.boolean :limpiar_casa
      t.boolean :responder_telefono

      t.timestamps null: false
    end
  end
end
