class CreateEvolucions < ActiveRecord::Migration
  def change
    create_table :evolucions do |t|
      t.references :paciente, index: true, foreign_key: true
      t.text :descripcion

      t.timestamps null: false
    end
  end
end
