class AddIsImportantToEvolucion < ActiveRecord::Migration
  def change
    add_column :evolucions, :is_important, :boolean, default: false
  end
end
