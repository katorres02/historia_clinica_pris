class CreateCuellos < ActiveRecord::Migration
  def change
    create_table :cuellos do |t|
      t.references :sistema_musculoesqueletico, index: true, foreign_key: true
      t.string :flexion_derecho
      t.string :flexion_izquierdo
      t.string :ext_derecho
      t.string :ext_izquierdo
      t.string :incl_lateral_der
      t.string :incl_lateral_izq
      t.string :rotacion_lat_der
      t.string :rotacion_lat_izq

      t.timestamps null: false
    end
  end
end
