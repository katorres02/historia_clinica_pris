class CreateInfos < ActiveRecord::Migration
  def change
    create_table :infos do |t|
      t.text :mensajes

      t.timestamps null: false
    end
  end
end
