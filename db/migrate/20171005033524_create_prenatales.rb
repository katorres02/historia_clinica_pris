class CreatePrenatales < ActiveRecord::Migration
  def change
    create_table :prenatales do |t|
      t.references :historia_clinica, index: true, foreign_key: true
      t.boolean :embarazo_riesgo
      t.boolean :planeado
      t.boolean :controles
      t.boolean :amenaza_aborto

      t.timestamps null: false
    end
  end
end
