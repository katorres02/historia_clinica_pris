class AddPrintToPrenatalePerinatalePostnatale < ActiveRecord::Migration
  def change
    add_column :prenatales, :print, :boolean
    add_column :perinatales, :print, :boolean
    add_column :postnatales, :print, :boolean
  end
end
