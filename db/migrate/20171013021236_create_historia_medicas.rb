class CreateHistoriaMedicas < ActiveRecord::Migration
  def change
    create_table :historia_medicas do |t|
      t.references :paciente, index: true, foreign_key: true
      t.text :motivo_consulta
      t.text :historia_actual
      t.text :antecedente_medico
      t.text :antecedente_quirurgico
      t.text :antecedente_ginecoobstetrico
      t.text :antecedente_alergia
      t.text :revision_sistemas
      t.text :imagenes
      t.text :laboratorios
      t.text :exploracion_fisica # Signos vitales
      t.text :inspeccion_general
      t.text :cabeza
      t.text :cuello
      t.text :torax
      t.text :abdomen
      t.text :genitales
      t.text :extremidades
      t.text :neurologico
      t.text :piel_faneras
      t.text :mental
      t.text :analisis
      t.text :plan
      t.text :intervencion_terapeutica
      t.text :formulacion
      t.text :control

      t.timestamps null: false
    end
  end
end
