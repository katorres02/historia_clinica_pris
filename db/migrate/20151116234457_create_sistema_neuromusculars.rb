class CreateSistemaNeuromusculars < ActiveRecord::Migration
  def change
    create_table :sistema_neuromusculars do |t|
      t.references :historia_clinica, index: true, foreign_key: true
      t.references :prueba_neuromuscular, index: true, foreign_key: true
      t.string :localizacion
      t.string :resultado
      t.string :estado_mental_espacio
      t.string :estado_mental_tiempo
      t.string :estado_mental_persona
      t.text :observacion

      t.timestamps null: false
    end
  end
end
