class AddFieldsToSistemaNeuromuscular < ActiveRecord::Migration
  def change
  	add_column :sistema_neuromusculars, :marcha, :string
  	add_column :sistema_neuromusculars, :propiocepcion, :string
  	add_column :sistema_neuromusculars, :equilibrio, :string
  	add_column :sistema_neuromusculars, :tono_muscular, :string
  end
end
