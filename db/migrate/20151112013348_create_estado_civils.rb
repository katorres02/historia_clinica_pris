class CreateEstadoCivils < ActiveRecord::Migration
  def change
    create_table :estado_civils do |t|
      t.string :name
      t.boolean :activo

      t.timestamps null: false
    end
  end
end
