class CreateTypeDocuments < ActiveRecord::Migration
  def change
    create_table :type_documents do |t|
      t.string :name
      t.boolean :status, default: true

      t.timestamps null: false
    end
  end
end
