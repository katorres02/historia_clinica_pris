class CreateHombros < ActiveRecord::Migration
  def change
    create_table :hombros do |t|
      t.references :sistema_musculoesqueletico, index: true, foreign_key: true
      t.string :flexion_derecho
      t.string :flexion_izquierdo
      t.string :ext_derecho
      t.string :ext_izquierdo
      t.string :abduccion_der
      t.string :abduccion_izq
      t.string :aduccion_der
      t.string :aduccion_izq
      t.string :rotacion_interna_der
      t.string :rotacion_interna_izq
      t.string :rotacion_externa_der
      t.string :rotacion_externa_izq

      t.timestamps null: false
    end
  end
end
