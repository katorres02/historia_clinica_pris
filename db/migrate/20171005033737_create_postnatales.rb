class CreatePostnatales < ActiveRecord::Migration
  def change
    create_table :postnatales do |t|
      t.references :historia_clinica, index: true, foreign_key: true
      t.boolean :complicaciones
      t.boolean :lactancia_exclusiva

      t.timestamps null: false
    end
  end
end
