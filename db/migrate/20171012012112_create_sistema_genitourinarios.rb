class CreateSistemaGenitourinarios < ActiveRecord::Migration
  def change
    create_table :sistema_genitourinarios do |t|
      t.references :historia_clinica, index: true, foreign_key: true
      t.boolean :indemne, default: true
      t.string :incontinencia
      t.boolean :prolapsos
      t.string :diastasis
      t.string :trastornos
      t.boolean :incontinencia_fecal
      t.boolean :estrenimiento
      t.boolean :disfuncion_sexual

      t.timestamps null: false
    end
  end
end
