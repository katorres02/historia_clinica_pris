class CreatePacientes < ActiveRecord::Migration
  def change
    create_table :pacientes do |t|
      t.references :usuario, index: true, foreign_key: true
      t.string :nombre
      t.date :fecha_nacimiento
      t.references :type_document, index: true, foreign_key: true
      t.string :documento
      t.integer :edad
      t.string :genero
      t.references :estado_civil, index: true, foreign_key: true
      t.string :escolaridad
      t.string :profesion
      t.string :direccion
      t.string :telefono
      t.string :eps
      
      t.string :email
      
      t.date :fecha_actual

      t.string :movil
      t.string :rh

      t.string :nombre_acompanante
      t.string :movil_acompanante
      t.string :remitido_por

      t.timestamps null: false
    end
  end
end
