class CreateCoordinacionMotoras < ActiveRecord::Migration
  def change
    create_table :coordinacion_motoras do |t|
      t.references :sistema_neuromuscular, index: true, foreign_key: true
      t.boolean :mano_boca
      t.boolean :mano_pie
      t.boolean :linea_media
      t.boolean :toma_objeto
      t.boolean :toma_mas
      t.boolean :prueba_diadococinesias
      t.boolean :prueba_dedo_nariz
      t.string :observaciones

      t.timestamps null: false
    end
  end
end
