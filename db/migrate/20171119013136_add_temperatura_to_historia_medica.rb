class AddTemperaturaToHistoriaMedica < ActiveRecord::Migration
  def change
    add_column :historia_medicas, :temperatura, :string
  end
end
