class CreatePruebaNeuromusculars < ActiveRecord::Migration
  def change
    create_table :prueba_neuromusculars do |t|
      t.string :nombre

      t.timestamps null: false
    end
  end
end
