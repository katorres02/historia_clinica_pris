class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :email
      t.string :password
      t.boolean :activo, default: false
      t.string :nombre
      t.string :apellido
      t.string :nombre_empresa
      t.string :logo_empresa
      t.string :documento
      t.string :tarjeta_profesional
      t.string :firma_digital
      t.string :telefono
      t.string :movil
      t.string :direccion

      t.timestamps null: false
    end
  end
end
