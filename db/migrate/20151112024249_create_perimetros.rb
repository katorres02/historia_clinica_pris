class CreatePerimetros < ActiveRecord::Migration
  def change
    create_table :perimetros do |t|
      t.references :sistema_musculoesqueletico, index: true, foreign_key: true
      t.float :biceps
      t.float :pecho
      t.float :abdomen
      t.float :cadera
      t.float :muslo
      t.float :pierna

      t.timestamps null: false
    end
  end
end
