#----------------------------------- SEMILLA DE DATOS MAESTROS ---------------------------------

# Autor: Carlos Andres Torres Cruz
#
# Fecha creacion: 21-11-2015
#
# Autor actualizacion:
#
# Fecha actualizacion:
#
# Semilla para estados civiles
EstadoCivil.create([
   { name: 'Casado', activo: true },
   { name: 'Soltero', activo: true }
 ])

# Autor: Carlos Andres Torres Cruz
#
# Fecha creacion: 03-10-2017
#
# Autor actualizacion:
#
# Fecha actualizacion:
#
# Semilla para tipos de documento
TypeDocument.create([
   { name: 'Tarjeta de identidad', status: true },
   { name: 'Cédula', status: true },
   { name: 'Pasaporte', status: true }
])

# Autor: Carlos Andres Torres Cruz
#
# Fecha creacion: 03-10-2017
#
# Autor actualizacion:
#
# Fecha actualizacion:
#
# Semilla para servicios del proyecto
Service.create([
   { name: 'Médico', status: true },
   { name: 'Fisioterapia', status: true }
])

# Autor: Carlos Andres Torres Cruz
#
# Fecha creacion: 16-11-2015
#
# Autor actualizacion:
#
# Fecha actualizacion:
#
# Semilla para usuarios
Usuario.create([
   { activo: true, nombre: 'Gustavo',  apellido: 'Salud', email: 'gustavo@mail.com', password: '12345678',
      nombre_empresa: 'Empresa 1', documento:'10567374734', tarjeta_profesional:'TP029323092', telefono: '89331122', movil: '3113334455', direccion: 'cra 7 # 8 - 9',
      logo_empresa:'https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Ruby_logo.svg/1000px-Ruby_logo.svg.png', can_edit: true },
   { activo: true, nombre: 'Priscila', apellido: 'Salud', email: 'pris@mail.com',    password: '12345678',
      nombre_empresa: 'Empresa 2', documento:'10546390093', tarjeta_profesional:'TP8728329839', telefono: '89331122', movil: '3113334455', direccion: 'cra 7 # 8 - 9',
      logo_empresa:'http://3.bp.blogspot.com/-PTty3CfTGnA/TpZOEjTQ_WI/AAAAAAAAAeo/KeKt_D5X2xo/s1600/js.jpg', can_edit: true }
])

# Autor: Carlos Andres Torres Cruz
#
# Fecha creacion: 03-10-2017
#
# Autor actualizacion:
#
# Fecha actualizacion:
#
# Semilla para servicios del proyecto
UserService.create([
   { usuario_id: 1, service_id: 1 },
   { usuario_id: 2, service_id: 2 }
])

# Autor: Vanessa Pineda Diaz
#
# Fecha creacion: 16-11-2015
#
# Autor actualizacion:
#
# Fecha actualizacion:
#
# Semilla para pacientes
Paciente.create([
   { usuario_id: 2, nombre: 'Tiger Nixon', documento: '30292390', telefono: '829382', movil: '324516263', fecha_actual: '12/10/2015' },
   { usuario_id: 2, nombre: 'Garrett Winters', documento: '10823784348', telefono: '8937860', movil: '31238439', fecha_actual: '21/09/2015'  },
   { usuario_id: 2, nombre: 'Ashton Cox', documento: '4388292', telefono: '8736437', movil: '312384382', fecha_actual: '01/10/2015'  },
   { usuario_id: 2, nombre: 'Cedric Kelly', documento: '10934884', telefono: '8989238', movil: '310238439', fecha_actual: '11/04/2015'  },
   { usuario_id: 2, nombre: 'Airi Satou', documento: '43435677', telefono: '8776656', movil: '321993800', fecha_actual: '08/07/2015'  },
   { usuario_id: 2, nombre: 'Brielle Williamson', documento: '34455456', telefono: '8909765', movil: '3203834090', fecha_actual: '12/10/2015'  },
   { usuario_id: 2, nombre: 'Herrod Chandler', documento: '43466567', telefono: '909877889', movil: '311933897', fecha_actual: '11/05/2015'  },
   { usuario_id: 2, nombre: 'Rhona Davidson', documento: '34434566', telefono: '87889909', movil: '315991523', fecha_actual: '12/10/2015'  },
   { usuario_id: 2, nombre: 'Colleen Hurst', documento: '348998777', telefono: '8899777', movil: '315236420', fecha_actual: '2009/09/15'  },
   { usuario_id: 2, nombre: 'Taylor Nixon', documento: '4503892', telefono: '8909322', movil: '320293939', fecha_actual: '11/11/2015' },
   { usuario_id: 2, nombre: 'Gart Summer', documento: '105367292', telefono: '8912123', movil: '3162938909', fecha_actual: '21/02/2015'  },
   { usuario_id: 2, nombre: 'Jennifer Cox', documento: '4829821', telefono: '8793482', movil: '312388394', fecha_actual: '02/04/2015'  },
])

# Autor: Carlos Andres Torres Cruz
#
# Fecha creacion: 21-11-2015
#
# Autor actualizacion:
#
# Fecha actualizacion:
#
# Semilla para administradores
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

# Autor: Carlos Andres Torres Cruz
#
# Fecha creacion: 30-11-2015
#
# Autor actualizacion:
#
# Fecha actualizacion:
#
# Semilla para pruebas especiales
PruebasEspecial.create([
   { nombre: 'Cajon' },
   { nombre: 'Bostezo' },
   { nombre: 'Adams' },
   { nombre: 'Escoliosis' }
   ])

# Autor: Carlos Andres Torres Cruz
#
# Fecha creacion: 06-12-2015
#
# Autor actualizacion:
#
# Fecha actualizacion:
#
# Semilla para pruebas flexibilidad
PruebasFlexibilidad.create([
   { nombre: 'Prueba 1'},
   { nombre: 'Prueba 3'},
   { nombre: 'Prueba 4'},
   { nombre: 'Prueba 5'},
   ])

# Autor: Carlos Andres Torres Cruz
#
# Fecha creacion: 15-12-2015
#
# Autor actualizacion:
#
# Fecha actualizacion:
#
# Semilla para pruebas neuromusculares
PruebaNeuromuscular.create([
   { nombre: 'SENSIBILIDAD'},
   { nombre: 'ROT'},
   { nombre: 'PARESIAS'},
   { nombre: 'TONO MUSCULAR'},
   { nombre: 'MARCHA'},
   { nombre: 'EQUILIBRIO'},
   { nombre: 'PROPIOCEPCION'}
])

