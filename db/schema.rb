# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180129220713) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "actividads", force: :cascade do |t|
    t.integer  "sistema_neuromuscular_id"
    t.boolean  "vestirse"
    t.boolean  "tomar_banio"
    t.boolean  "levantar_sentar"
    t.boolean  "subir_bajar_escalera"
    t.boolean  "subir_bajar_rampa"
    t.boolean  "caminar_barrio"
    t.boolean  "preparar_comida"
    t.boolean  "limpiar_casa"
    t.boolean  "responder_telefono"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "historia_clinica_id"
  end

  add_index "actividads", ["historia_clinica_id"], name: "index_actividads_on_historia_clinica_id", using: :btree
  add_index "actividads", ["sistema_neuromuscular_id"], name: "index_actividads_on_sistema_neuromuscular_id", using: :btree

  create_table "adipometria", force: :cascade do |t|
    t.integer  "sistema_musculoesqueletico_id"
    t.float    "triceps"
    t.float    "s_escap"
    t.float    "s_iliaco"
    t.float    "abdomen"
    t.float    "muslo"
    t.float    "pierna"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "adipometria", ["sistema_musculoesqueletico_id"], name: "index_adipometria_on_sistema_musculoesqueletico_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "caderas", force: :cascade do |t|
    t.integer  "sistema_musculoesqueletico_id"
    t.string   "flexion_der"
    t.string   "flexion_izq"
    t.string   "extension_der"
    t.string   "extension_izq"
    t.string   "abduccion_der"
    t.string   "abduccion_izq"
    t.string   "aduccion_der"
    t.string   "aduccion_izq"
    t.string   "rotacion_medial_der"
    t.string   "rotacion_medial_izq"
    t.string   "rotacion_lateral_der"
    t.string   "rotacion_lateral_izq"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "caderas", ["sistema_musculoesqueletico_id"], name: "index_caderas_on_sistema_musculoesqueletico_id", using: :btree

  create_table "clasificacion_internacionals", force: :cascade do |t|
    t.integer  "sistema_neuromuscular_id"
    t.string   "deficiencia_principal"
    t.string   "limitacion_actividades"
    t.string   "factores_ambientales"
    t.string   "observacion"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "historia_clinica_id"
  end

  add_index "clasificacion_internacionals", ["historia_clinica_id"], name: "index_clasificacion_internacionals_on_historia_clinica_id", using: :btree
  add_index "clasificacion_internacionals", ["sistema_neuromuscular_id"], name: "index_clasificacion_internacionals_on_sistema_neuromuscular_id", using: :btree

  create_table "contactos", force: :cascade do |t|
    t.string   "nombre"
    t.string   "email"
    t.string   "asunto"
    t.text     "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "coordinacion_motoras", force: :cascade do |t|
    t.integer  "sistema_neuromuscular_id"
    t.boolean  "mano_boca"
    t.boolean  "mano_pie"
    t.boolean  "linea_media"
    t.boolean  "toma_objeto"
    t.boolean  "toma_mas"
    t.boolean  "prueba_diadococinesias"
    t.boolean  "prueba_dedo_nariz"
    t.string   "observaciones"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "historia_clinica_id"
  end

  add_index "coordinacion_motoras", ["historia_clinica_id"], name: "index_coordinacion_motoras_on_historia_clinica_id", using: :btree
  add_index "coordinacion_motoras", ["sistema_neuromuscular_id"], name: "index_coordinacion_motoras_on_sistema_neuromuscular_id", using: :btree

  create_table "cuellos", force: :cascade do |t|
    t.integer  "sistema_musculoesqueletico_id"
    t.string   "flexion_derecho"
    t.string   "flexion_izquierdo"
    t.string   "ext_derecho"
    t.string   "ext_izquierdo"
    t.string   "incl_lateral_der"
    t.string   "incl_lateral_izq"
    t.string   "rotacion_lat_der"
    t.string   "rotacion_lat_izq"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "cuellos", ["sistema_musculoesqueletico_id"], name: "index_cuellos_on_sistema_musculoesqueletico_id", using: :btree

  create_table "daniels", force: :cascade do |t|
    t.string   "musculo_cero"
    t.string   "musculo_uno"
    t.string   "musculo_dos"
    t.string   "musculo_tres"
    t.string   "musculo_cuatro"
    t.string   "musculo_cinco"
    t.integer  "sistema_musculoesqueletico_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "desarrollo_motors", force: :cascade do |t|
    t.integer  "historia_clinica_id"
    t.string   "centro_cefalico"
    t.string   "sedente"
    t.string   "rolado"
    t.string   "gateo"
    t.string   "bipedo"
    t.string   "marcha"
    t.string   "adicion"
    t.string   "antecedentes_psicosociales"
    t.string   "nucleo_familiar"
    t.string   "escolaridad"
    t.string   "habitos"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.text     "observacion"
  end

  add_index "desarrollo_motors", ["historia_clinica_id"], name: "index_desarrollo_motors_on_historia_clinica_id", using: :btree

  create_table "diagnosticos", force: :cascade do |t|
    t.integer  "historia_clinica_id"
    t.string   "diagnostico"
    t.string   "recomendacion"
    t.string   "restriccion"
    t.string   "sugerencia_terapeutica"
    t.string   "objetivo_general"
    t.string   "objetivo_especifico"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "diagnosticos", ["historia_clinica_id"], name: "index_diagnosticos_on_historia_clinica_id", using: :btree

  create_table "dolors", force: :cascade do |t|
    t.integer  "sistema_musculoesqueletico_id"
    t.string   "localizacion"
    t.string   "tipo"
    t.string   "reposo"
    t.string   "palpacion"
    t.string   "movimiento"
    t.string   "resistencia"
    t.string   "exaservacion"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "dolors", ["sistema_musculoesqueletico_id"], name: "index_dolors_on_sistema_musculoesqueletico_id", using: :btree

  create_table "edemas", force: :cascade do |t|
    t.integer  "sistema_musculoesqueletico_id"
    t.string   "localizacion"
    t.float    "derecho"
    t.float    "izquierdo"
    t.float    "total"
    t.string   "grado"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "edemas", ["sistema_musculoesqueletico_id"], name: "index_edemas_on_sistema_musculoesqueletico_id", using: :btree

  create_table "estado_civils", force: :cascade do |t|
    t.string   "name"
    t.boolean  "activo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "evolucions", force: :cascade do |t|
    t.integer  "paciente_id"
    t.text     "descripcion"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.date     "creation_date"
    t.boolean  "is_important",  default: false
  end

  add_index "evolucions", ["paciente_id"], name: "index_evolucions_on_paciente_id", using: :btree

  create_table "fuerza_musculars", force: :cascade do |t|
    t.integer  "sistema_musculoesqueletico_id"
    t.string   "cuello_i"
    t.string   "cuello_d"
    t.string   "hombro_i"
    t.string   "hombro_d"
    t.string   "codo_i"
    t.string   "codo_d"
    t.string   "muneca_i"
    t.string   "muneca_d"
    t.string   "tronco_i"
    t.string   "tronco_d"
    t.string   "miembro_superior_i"
    t.string   "miembro_superior_d"
    t.string   "miembro_inferior_i"
    t.string   "miembro_inferior_d"
    t.boolean  "ayudas"
    t.string   "ayudas_text"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "fuerza_musculars", ["sistema_musculoesqueletico_id"], name: "index_fuerza_musculars_on_sistema_musculoesqueletico_id", using: :btree

  create_table "historia_clinicas", force: :cascade do |t|
    t.integer  "paciente_id"
    t.text     "antecedentes_personales"
    t.text     "cirugias"
    t.text     "examenes_clinicos"
    t.integer  "embarazos"
    t.integer  "partos"
    t.integer  "cesarea"
    t.integer  "hijos_vivos"
    t.integer  "abortos"
    t.text     "fum"
    t.string   "embarazo_actual"
    t.text     "medicamentos"
    t.text     "antecedentes_familiares"
    t.text     "motivo_consulta"
    t.string   "tiempo_gestacion"
    t.boolean  "riesgo_embarazo",         default: false
    t.text     "riesgo_embarazo_razon"
    t.text     "patologicos"
    t.text     "hospitalarios"
    t.text     "alergicos"
    t.text     "ayudas_previas"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "historia_clinicas", ["paciente_id"], name: "index_historia_clinicas_on_paciente_id", using: :btree

  create_table "historia_medicas", force: :cascade do |t|
    t.integer  "paciente_id"
    t.text     "motivo_consulta"
    t.text     "historia_actual"
    t.text     "antecedente_medico"
    t.text     "antecedente_quirurgico"
    t.text     "antecedente_ginecoobstetrico"
    t.text     "antecedente_alergia"
    t.text     "revision_sistemas"
    t.text     "imagenes"
    t.text     "laboratorios"
    t.text     "exploracion_fisica"
    t.text     "inspeccion_general"
    t.text     "cabeza"
    t.text     "cuello"
    t.text     "torax"
    t.text     "abdomen"
    t.text     "genitales"
    t.text     "extremidades"
    t.text     "neurologico"
    t.text     "piel_faneras"
    t.text     "mental"
    t.text     "analisis"
    t.text     "plan"
    t.text     "intervencion_terapeutica"
    t.text     "formulacion"
    t.text     "control"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "ta"
    t.string   "fc"
    t.string   "fr"
    t.string   "sato2"
    t.string   "peso"
    t.string   "talla"
    t.string   "glucometria"
    t.string   "imc"
    t.string   "temperatura"
    t.integer  "unidad_talla",                 default: 1
  end

  add_index "historia_medicas", ["paciente_id"], name: "index_historia_medicas_on_paciente_id", using: :btree

  create_table "hombros", force: :cascade do |t|
    t.integer  "sistema_musculoesqueletico_id"
    t.string   "flexion_derecho"
    t.string   "flexion_izquierdo"
    t.string   "ext_derecho"
    t.string   "ext_izquierdo"
    t.string   "abduccion_der"
    t.string   "abduccion_izq"
    t.string   "aduccion_der"
    t.string   "aduccion_izq"
    t.string   "rotacion_interna_der"
    t.string   "rotacion_interna_izq"
    t.string   "rotacion_externa_der"
    t.string   "rotacion_externa_izq"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "hombros", ["sistema_musculoesqueletico_id"], name: "index_hombros_on_sistema_musculoesqueletico_id", using: :btree

  create_table "impresion_diagnosticas", force: :cascade do |t|
    t.integer  "historia_medica_id"
    t.text     "descripcion"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "impresion_diagnosticas", ["historia_medica_id"], name: "index_impresion_diagnosticas_on_historia_medica_id", using: :btree

  create_table "infos", force: :cascade do |t|
    t.text     "mensajes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memoria", force: :cascade do |t|
    t.integer  "sistema_neuromuscular_id"
    t.string   "anterograda"
    t.string   "retrogada"
    t.string   "queja_memoria"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "historia_clinica_id"
  end

  add_index "memoria", ["historia_clinica_id"], name: "index_memoria_on_historia_clinica_id", using: :btree
  add_index "memoria", ["sistema_neuromuscular_id"], name: "index_memoria_on_sistema_neuromuscular_id", using: :btree

  create_table "musculos_inferiors", force: :cascade do |t|
    t.integer  "sistema_musculoesqueletico_id"
    t.string   "flexion_rodilla_der"
    t.string   "flexion_rodilla_izq"
    t.string   "extension_der"
    t.string   "extension_izq"
    t.string   "dorsflexion_tobillo_der"
    t.string   "dorsflexion_tobillo_izq"
    t.string   "plantiflexion_der"
    t.string   "plantiflexion_izq"
    t.string   "inversion_tobillo_der"
    t.string   "inversion_tobillo_izq"
    t.string   "eversion_tobillo_der"
    t.string   "eversion_tobillo_izq"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "musculos_inferiors", ["sistema_musculoesqueletico_id"], name: "index_musculos_inferiors_on_sistema_musculoesqueletico_id", using: :btree

  create_table "musculos_superiors", force: :cascade do |t|
    t.integer  "sistema_musculoesqueletico_id"
    t.string   "flexion_codo_der"
    t.string   "flexion_codo_izq"
    t.string   "extension_codo_der"
    t.string   "extension_codo_izq"
    t.string   "supinacion_codo_der"
    t.string   "supinacion_codo_izq"
    t.string   "pronacion_codo_der"
    t.string   "pronacion_codo_izq"
    t.string   "extension_muneca_der"
    t.string   "extension_muneca_izq"
    t.string   "desviacion_ulnar_muneca_der"
    t.string   "desviacion_ulnar_muneca_izq"
    t.string   "desviacion_radial_muneca_der"
    t.string   "desviacion_radial_muneca_izq"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "musculos_superiors", ["sistema_musculoesqueletico_id"], name: "index_musculos_superiors_on_sistema_musculoesqueletico_id", using: :btree

  create_table "pacientes", force: :cascade do |t|
    t.integer  "usuario_id"
    t.string   "nombre"
    t.date     "fecha_nacimiento"
    t.integer  "type_document_id"
    t.string   "documento"
    t.integer  "edad"
    t.string   "genero"
    t.integer  "estado_civil_id"
    t.string   "escolaridad"
    t.string   "profesion"
    t.string   "direccion"
    t.string   "telefono"
    t.string   "eps"
    t.string   "email"
    t.date     "fecha_actual"
    t.string   "movil"
    t.string   "rh"
    t.string   "nombre_acompanante"
    t.string   "movil_acompanante"
    t.string   "remitido_por"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "pacientes", ["estado_civil_id"], name: "index_pacientes_on_estado_civil_id", using: :btree
  add_index "pacientes", ["type_document_id"], name: "index_pacientes_on_type_document_id", using: :btree
  add_index "pacientes", ["usuario_id"], name: "index_pacientes_on_usuario_id", using: :btree

  create_table "perimetros", force: :cascade do |t|
    t.integer  "sistema_musculoesqueletico_id"
    t.float    "biceps"
    t.float    "pecho"
    t.float    "abdomen"
    t.float    "cadera"
    t.float    "muslo"
    t.float    "pierna"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "perimetros", ["sistema_musculoesqueletico_id"], name: "index_perimetros_on_sistema_musculoesqueletico_id", using: :btree

  create_table "perinatales", force: :cascade do |t|
    t.integer  "historia_clinica_id"
    t.string   "nacimiento"
    t.boolean  "cesarias"
    t.boolean  "vaginal"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.boolean  "print"
  end

  add_index "perinatales", ["historia_clinica_id"], name: "index_perinatales_on_historia_clinica_id", using: :btree

  create_table "postnatales", force: :cascade do |t|
    t.integer  "historia_clinica_id"
    t.boolean  "complicaciones"
    t.boolean  "lactancia_exclusiva"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.boolean  "print"
  end

  add_index "postnatales", ["historia_clinica_id"], name: "index_postnatales_on_historia_clinica_id", using: :btree

  create_table "posturas", force: :cascade do |t|
    t.integer  "sistema_musculoesqueletico_id"
    t.string   "cabeza_cuello"
    t.string   "hombros"
    t.string   "tronco"
    t.string   "miembros_superiores"
    t.string   "miembros_inferiores"
    t.string   "columna"
    t.string   "pelvis"
    t.string   "observacion"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "posturas", ["sistema_musculoesqueletico_id"], name: "index_posturas_on_sistema_musculoesqueletico_id", using: :btree

  create_table "prenatales", force: :cascade do |t|
    t.integer  "historia_clinica_id"
    t.boolean  "embarazo_riesgo"
    t.boolean  "planeado"
    t.boolean  "controles"
    t.boolean  "amenaza_aborto"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.boolean  "print"
  end

  add_index "prenatales", ["historia_clinica_id"], name: "index_prenatales_on_historia_clinica_id", using: :btree

  create_table "prueba_neuromusculars", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pruebas_especiales_pacientes", force: :cascade do |t|
    t.integer  "historia_clinica_id"
    t.integer  "pruebas_especial_id"
    t.string   "zona"
    t.string   "derecho"
    t.string   "izquierdo"
    t.text     "observaciones"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "pruebas_especiales_pacientes", ["historia_clinica_id"], name: "index_pruebas_especiales_pacientes_on_historia_clinica_id", using: :btree
  add_index "pruebas_especiales_pacientes", ["pruebas_especial_id"], name: "index_pruebas_especiales_pacientes_on_pruebas_especial_id", using: :btree

  create_table "pruebas_especials", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pruebas_flexibilidad_pacientes", force: :cascade do |t|
    t.integer  "historia_clinica_id"
    t.integer  "pruebas_flexibilidad_id"
    t.string   "musculo"
    t.string   "positivo"
    t.string   "negativo"
    t.string   "leve"
    t.string   "moderado"
    t.string   "severo"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "pruebas_flexibilidad_pacientes", ["historia_clinica_id"], name: "index_pruebas_flexibilidad_pacientes_on_historia_clinica_id", using: :btree
  add_index "pruebas_flexibilidad_pacientes", ["pruebas_flexibilidad_id"], name: "index_pruebas_flexibilidad_pacientes_on_pruebas_flexibilidad_id", using: :btree

  create_table "pruebas_flexibilidads", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pruebas_posturas", force: :cascade do |t|
    t.integer  "historia_clinica_id"
    t.string   "espalda"
    t.string   "cinetica_inf_string"
    t.string   "cinetica_sup"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "pruebas_posturas", ["historia_clinica_id"], name: "index_pruebas_posturas_on_historia_clinica_id", using: :btree

  create_table "psicomotricidads", force: :cascade do |t|
    t.integer  "sistema_neuromuscular_id"
    t.boolean  "lanzamiento"
    t.boolean  "pateo"
    t.boolean  "recepcion"
    t.boolean  "velocidad"
    t.boolean  "tamborilear"
    t.boolean  "escritura"
    t.boolean  "salto_bipodal"
    t.boolean  "salto_unipodal"
    t.boolean  "ascenso_descenso"
    t.boolean  "pedaleo"
    t.boolean  "trepado"
    t.boolean  "carrera"
    t.string   "dominancia"
    t.boolean  "lateralidad"
    t.string   "nocion_cuerpo"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "historia_clinica_id"
  end

  add_index "psicomotricidads", ["historia_clinica_id"], name: "index_psicomotricidads_on_historia_clinica_id", using: :btree
  add_index "psicomotricidads", ["sistema_neuromuscular_id"], name: "index_psicomotricidads_on_sistema_neuromuscular_id", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "name"
    t.boolean  "status",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "sistema_cardiopulmonars", force: :cascade do |t|
    t.integer  "historia_clinica_id"
    t.string   "pa"
    t.string   "fc"
    t.string   "fr"
    t.string   "sao2"
    t.string   "ausc_pulmonar"
    t.string   "ausc_cardiaca"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "sistema_cardiopulmonars", ["historia_clinica_id"], name: "index_sistema_cardiopulmonars_on_historia_clinica_id", using: :btree

  create_table "sistema_genitourinarios", force: :cascade do |t|
    t.integer  "historia_clinica_id"
    t.boolean  "indemne",             default: true
    t.string   "incontinencia"
    t.boolean  "prolapsos"
    t.string   "diastasis"
    t.string   "trastornos"
    t.boolean  "incontinencia_fecal"
    t.boolean  "estrenimiento"
    t.boolean  "disfuncion_sexual"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.text     "observaciones"
  end

  add_index "sistema_genitourinarios", ["historia_clinica_id"], name: "index_sistema_genitourinarios_on_historia_clinica_id", using: :btree

  create_table "sistema_musculoesqueleticos", force: :cascade do |t|
    t.integer  "historia_clinica_id"
    t.float    "talla"
    t.float    "peso"
    t.float    "imc"
    t.float    "grasa_total"
    t.float    "grasa_localizada"
    t.float    "m_superior_r_derecho"
    t.float    "m_superior_r_izquierdo"
    t.float    "m_superior_a_derecho"
    t.float    "m_superior_a_izquierdo"
    t.float    "m_superiores_diferencia"
    t.float    "m_inferior_r_derecho"
    t.float    "m_inferior_r_izquierdo"
    t.float    "m_inferior_a_derecho"
    t.float    "m_inferior_a_izquierdo"
    t.float    "m_inferiores_diferencia"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "unidad_talla",            default: 1
  end

  add_index "sistema_musculoesqueleticos", ["historia_clinica_id"], name: "index_sistema_musculoesqueleticos_on_historia_clinica_id", using: :btree

  create_table "sistema_neuromusculars", force: :cascade do |t|
    t.integer  "historia_clinica_id"
    t.integer  "prueba_neuromuscular_id"
    t.string   "localizacion"
    t.string   "resultado"
    t.string   "estado_mental_espacio"
    t.string   "estado_mental_tiempo"
    t.string   "estado_mental_persona"
    t.text     "observacion"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "marcha"
    t.string   "propiocepcion"
    t.string   "equilibrio"
    t.string   "tono_muscular"
  end

  add_index "sistema_neuromusculars", ["historia_clinica_id"], name: "index_sistema_neuromusculars_on_historia_clinica_id", using: :btree
  add_index "sistema_neuromusculars", ["prueba_neuromuscular_id"], name: "index_sistema_neuromusculars_on_prueba_neuromuscular_id", using: :btree

  create_table "sistema_neurosensorials", force: :cascade do |t|
    t.integer  "sistema_neuromuscular_id"
    t.boolean  "vestirse"
    t.boolean  "disfruta_banio"
    t.boolean  "cepilla"
    t.boolean  "molesta_ensucia"
    t.boolean  "molesta_sonido"
    t.boolean  "busca_sonido"
    t.boolean  "responde_llamado"
    t.boolean  "disfruta_estimulo"
    t.boolean  "persecucion_ocular"
    t.boolean  "superficie_inestable"
    t.boolean  "juegos_mecanicos"
    t.boolean  "juegos_aparatos"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "historia_clinica_id"
  end

  add_index "sistema_neurosensorials", ["historia_clinica_id"], name: "index_sistema_neurosensorials_on_historia_clinica_id", using: :btree
  add_index "sistema_neurosensorials", ["sistema_neuromuscular_id"], name: "index_sistema_neurosensorials_on_sistema_neuromuscular_id", using: :btree

  create_table "sistema_tegumentarios", force: :cascade do |t|
    t.integer  "historia_clinica_id"
    t.integer  "tipos_tegumentario_id"
    t.string   "color"
    t.string   "estado"
    t.string   "edema"
    t.boolean  "tumefaccion",           default: false
    t.boolean  "escaras",               default: false
    t.boolean  "heridas",               default: false
    t.string   "cicatriz"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "sistema_tegumentarios", ["historia_clinica_id"], name: "index_sistema_tegumentarios_on_historia_clinica_id", using: :btree
  add_index "sistema_tegumentarios", ["tipos_tegumentario_id"], name: "index_sistema_tegumentarios_on_tipos_tegumentario_id", using: :btree

  create_table "tipos_tegumentarios", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transicions", force: :cascade do |t|
    t.integer  "sistema_neuromuscular_id"
    t.string   "decubito_prono"
    t.string   "supino"
    t.string   "decubito_lateral"
    t.string   "sedente_bipedo"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "historia_clinica_id"
  end

  add_index "transicions", ["historia_clinica_id"], name: "index_transicions_on_historia_clinica_id", using: :btree
  add_index "transicions", ["sistema_neuromuscular_id"], name: "index_transicions_on_sistema_neuromuscular_id", using: :btree

  create_table "troncos", force: :cascade do |t|
    t.integer  "sistema_musculoesqueletico_id"
    t.string   "flexion_der"
    t.string   "flexion_izq"
    t.string   "extension_der"
    t.string   "extension_izq"
    t.string   "flexion_lateral_der"
    t.string   "flexion_lateral_izq"
    t.string   "rotacion_der"
    t.string   "rotacion_izq"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "troncos", ["sistema_musculoesqueletico_id"], name: "index_troncos_on_sistema_musculoesqueletico_id", using: :btree

  create_table "type_documents", force: :cascade do |t|
    t.string   "name"
    t.boolean  "status",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "user_services", force: :cascade do |t|
    t.integer  "service_id"
    t.integer  "usuario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_services", ["service_id"], name: "index_user_services_on_service_id", using: :btree
  add_index "user_services", ["usuario_id"], name: "index_user_services_on_usuario_id", using: :btree

  create_table "usuarios", force: :cascade do |t|
    t.string   "email"
    t.string   "password"
    t.boolean  "activo",                 default: false
    t.string   "nombre"
    t.string   "apellido"
    t.string   "nombre_empresa"
    t.string   "logo_empresa"
    t.string   "documento"
    t.string   "tarjeta_profesional"
    t.string   "firma_digital"
    t.string   "telefono"
    t.string   "movil"
    t.string   "direccion"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.boolean  "can_edit"
  end

  add_index "usuarios", ["email"], name: "index_usuarios_on_email", unique: true, using: :btree
  add_index "usuarios", ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "actividads", "sistema_neuromusculars"
  add_foreign_key "adipometria", "sistema_musculoesqueleticos"
  add_foreign_key "caderas", "sistema_musculoesqueleticos"
  add_foreign_key "clasificacion_internacionals", "sistema_neuromusculars"
  add_foreign_key "coordinacion_motoras", "sistema_neuromusculars"
  add_foreign_key "cuellos", "sistema_musculoesqueleticos"
  add_foreign_key "desarrollo_motors", "historia_clinicas"
  add_foreign_key "diagnosticos", "historia_clinicas"
  add_foreign_key "dolors", "sistema_musculoesqueleticos"
  add_foreign_key "edemas", "sistema_musculoesqueleticos"
  add_foreign_key "evolucions", "pacientes"
  add_foreign_key "fuerza_musculars", "sistema_musculoesqueleticos"
  add_foreign_key "historia_clinicas", "pacientes"
  add_foreign_key "historia_medicas", "pacientes"
  add_foreign_key "hombros", "sistema_musculoesqueleticos"
  add_foreign_key "impresion_diagnosticas", "historia_medicas"
  add_foreign_key "memoria", "sistema_neuromusculars"
  add_foreign_key "musculos_inferiors", "sistema_musculoesqueleticos"
  add_foreign_key "musculos_superiors", "sistema_musculoesqueleticos"
  add_foreign_key "pacientes", "estado_civils"
  add_foreign_key "pacientes", "type_documents"
  add_foreign_key "pacientes", "usuarios"
  add_foreign_key "perimetros", "sistema_musculoesqueleticos"
  add_foreign_key "perinatales", "historia_clinicas"
  add_foreign_key "postnatales", "historia_clinicas"
  add_foreign_key "posturas", "sistema_musculoesqueleticos"
  add_foreign_key "prenatales", "historia_clinicas"
  add_foreign_key "pruebas_especiales_pacientes", "historia_clinicas"
  add_foreign_key "pruebas_especiales_pacientes", "pruebas_especials"
  add_foreign_key "pruebas_flexibilidad_pacientes", "historia_clinicas"
  add_foreign_key "pruebas_flexibilidad_pacientes", "pruebas_flexibilidads"
  add_foreign_key "pruebas_posturas", "historia_clinicas"
  add_foreign_key "psicomotricidads", "sistema_neuromusculars"
  add_foreign_key "sistema_cardiopulmonars", "historia_clinicas"
  add_foreign_key "sistema_genitourinarios", "historia_clinicas"
  add_foreign_key "sistema_musculoesqueleticos", "historia_clinicas"
  add_foreign_key "sistema_neuromusculars", "historia_clinicas"
  add_foreign_key "sistema_neuromusculars", "prueba_neuromusculars"
  add_foreign_key "sistema_neurosensorials", "sistema_neuromusculars"
  add_foreign_key "sistema_tegumentarios", "historia_clinicas"
  add_foreign_key "sistema_tegumentarios", "tipos_tegumentarios"
  add_foreign_key "transicions", "sistema_neuromusculars"
  add_foreign_key "troncos", "sistema_musculoesqueleticos"
  add_foreign_key "user_services", "services"
  add_foreign_key "user_services", "usuarios"
end
