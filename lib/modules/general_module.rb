# Modulo general
module GeneralModule
  # Autor: Jackson Florez Jimenez
  #
  # Fecha creacion: 24-05-2013
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Metodo: que se usa para dominios cruzados en servidores dejando
  # que las petisiones sean existosas para la parte movil
  def set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Request-Method'] = %w{GET POST OPTIONS HEAD}.join(',')
  end

  # Autor: Jackson Florez Jimenez
  #
  # Fecha creacion: 09-11-2015
  #
  # Autor actualizacion:
  #
  # Fecha actualizacion:
  #
  # Metodo: de prueba para revisar el funcionamiendo
  # de la arquitectura DDD(Domain Driven Design)
  def example_method_ddd(input_one, input_two)
    input_one.to_i+input_two.to_i
  end
end