require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ReferencesProyect
  class Application < Rails::Application

    # Se configura por defecto se utilice haml enves de erb
    config.generators do |g|
      g.template_engine :haml
    end

    config.generators do |g|
      g.orm :active_record
    end

    # Se Configura la arquitectura DDD(Domain Driven Design)
    config.autoload_paths += %W(#{config.root}/lib/modules)

    # Se Configura carga de estilos
    config.assets.enabled = true
    config.active_record.raise_in_transactional_callbacks = true
    # Carga de fuentes
    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')

    # configuración para envío de imágenes locales para los correos
    # config.action_mailer.asset_host = 'http://45.55.78.42'

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'
    config.time_zone = 'Bogota'

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true
  end
end
