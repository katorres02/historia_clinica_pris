Rails.application.routes.draw do

  devise_for :usuarios, controllers: { sessions: 'devise_logins' }
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_scope :usuarios do
    authenticated  do
      root to: 'patients#index'
    end

    unauthenticated do
      root to: 'users#login', as: 'unauthenticated_root'
    end
  end

  resources :patients do
    collection do
      get 'print'
    end
    resources :evolutions, except: [:show]
    collection do
      patch 'update'
      get 'clinic_history'
      post 'create_clinic_history'
      patch 'update_clinic_history'
      get 'search_data'
      post 'import_data'
    end
  end

  resources :cardiopulmonar, only: [:create, :update] do
    collection do
      patch 'update'
    end
  end

  resources :musculoesqueletico, only: [:create, :update] do
    collection do
      patch 'update'
    end
  end

  resources :services  do
    collection do
      post 'save_company_logo'
      get 'specials_tests'
      get 'flex_tests'
      get 'neuro_test'
    end
  end

  # Nuevas funcionalidades 03-10-2017

  namespace :fisio do
    resources :patients do
      resources :evoluciones
      resources :clinic_histories, except: [:destroy] do
        resources :desarrollo_motores, except: [:index, :show, :destroy]
        resources :sistema_cardiopulmonar, except: [:index, :show, :destroy]
        resources :sistema_musculoesqueletico, except: [:index, :show, :destroy]
        resources :sistema_neuromuscular, except: [:index, :show, :destroy] do
          collection do
            post 'create_extra'
            put 'update_extra'
          end
        end
        resources :remision_externa, except: [:index, :show, :destroy]
        resources :sistema_tegumentario, except: [:index, :show, :destroy]
        resources :pruebas_especiales, except: [:index, :show, :destroy]
        resources :sistema_genitourinario, except: [:index, :show, :destroy]
        resources :print, except: [:index, :new, :show, :create, :edit, :update, :destroy] do
          collection do
            get 'single'
          end
        end
      end
    end
  end

  namespace :medical do
    resources :patients do
      resources :historia_medicas, except: [:destroy] do
        resources :print, except: [:index, :new, :show, :create, :edit, :update, :destroy] do
          collection do
            get 'single'
            get 'formulacion'
          end
        end
      end
    end
  end
end
